import numpy as np 

import matplotlib.pyplot as plt 

figure = plt.gcf()
figure.set_size_inches(20,10)
iarr = [0,1000,2000,3000]
for ii, i in enumerate(iarr):
	state = np.loadtxt('tecplotdatafile_%06d.dat'%i,skiprows=2)
	x = state[:,0]
	rho = state[:,1]
	u = state[:,2]
	p = state[:,3]
	phi = state[:,6]
	a = state[:,9]
	
	plt.subplot(2,3,1)
	plt.title('RHO')
	plt.plot(x,rho)
		
	plt.subplot(2,3,2)
	plt.title('U')
	plt.plot(x,u)	
	
	plt.subplot(2,3,3)
	plt.title('P')
	plt.plot(x,p)
	
	plt.subplot(2,3,4)
	plt.title('PHI')
	plt.plot(x,phi)	
	
	plt.subplot(2,3,5)
	plt.title('Speed of sound')
	plt.plot(x,a)
	
	fname = '%06d.png'%ii
	plt.savefig(fname)
	plt.clf()



plt.savefig('results.png',dpi = 120)
