import numpy as np 


import matplotlib 
matplotlib.use('Agg') 
import matplotlib.pyplot as plt

figure = plt.gcf()
figure.set_size_inches(20,10)
for i in [0,1000,2000,3000]:
	state = np.loadtxt('tecplotdatafile_%06d.dat'%i,skiprows=2)
	x = state[:,0]
	rho = state[:,1]
	u = state[:,2]
	p = state[:,3]
	phi1 = state[:,8]
	phi2 = state[:,9]
	
	plt.subplot(2,3,1)
	plt.title('RHO')
	plt.plot(x,rho)
	#plt.legend(['t=0','t=3'])
	plt.legend(['t = 0', 't= 1', 't = 2', 't = 3'])
	
	plt.subplot(2,3,2)
	plt.title('U')
	plt.plot(x,u)	
	plt.legend(['t = 0', 't= 1', 't = 2', 't = 3'])
	#plt.legend(['t=0','t=3'])
	
	
	plt.subplot(2,3,3)
	plt.title('P')
	plt.plot(x,p)
	#plt.legend(['t=0','t=3'])
	plt.legend(['t = 0', 't= 1', 't = 2', 't = 3'])
	
	plt.subplot(2,3,4)
	plt.title('PHI')
	plt.plot(x,phi1)
	plt.plot(x,phi2)
	#plt.legend(['t=0','t=3'])	
	plt.legend(['t = 0', 't= 1', 't = 2', 't = 3'])

	
	


plt.savefig('results.png',dpi = 120)

#print(np.shape(phi1))
#print(np.shape(phi2))





