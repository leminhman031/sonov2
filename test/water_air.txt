! RESTART SECTION
0    ! 0=no restart, 1=restart
100  ! iteration value of restart file to be used if restarting

! OUTPUT FOR DATAFILES AND TERMINAL
1  ! 0=matlab,1=tecplot,2=serial-vtk-rectilinear-grid format
50   ! frequency to print dataline on terminal window
500   ! frequency to print new table title on terminal window

! NUMERICS VARIABLES
5000000   ! maximum walltime (secs); currently set to 5 days
3000  2000000   ! maxiter; maximum iteration number for the computation
1000  1000000       ! outiter; iteration frequency to write output datafiles
1705000       ! outrst; iteration frequency to write restart datafile
2.0       ! outtau; (t/tau) frequency to write output datafile
0      ! iter_reinit; max iteration for reinitialization scheme
0      ! iterT; max iteration for temperature correction; if 0 T is ignored
1   ! intiter; sets iteration frequency for shukla2010 interface correction
20.5    ! finaltime; final time of the simulation (in t/tau (nondim))
5.00E-6   ! deltat; the time step for the simulation
0.2    ! CFL; the value of CFL condition used to recalculate deltat
3   ! rk_reinit; RK order for reinitialization scheme
1.0   ! kfish; multiplier to fisher shape correction

! SCALING VARIABLES (FOR NONDIM)
0.500E-6      ! lscale is length scale in meters
1.00E5        ! pscale is pressure scale in Pa
1000.0        ! rscale is density scale in kg/m3
300.00        ! Tempscale is temperature scale in kelvin

! GRID VARIABLES
-4.0   ! xmin is lower axial bound of problem domain
4.0   ! xmax is upper axial bound of problem domain
-0.0008   ! ymin is lower transverse bound of problem domain
0.0008   ! ymax is upper transverse bound of problem domain
-0.0008   ! zmin is lower transverse bound of problem domain
0.0008   ! zmax is upper transverse bound of problem domain
1000   ! NI is # of divisions in axial direction
1       ! NJ is # of divisions in transverse direction
1       ! NK is # of divisions in transverse direction
2    ! NG is # of ghost cells outside of problem domain

! FLOWFIELD VARIABLES
1  ! nshock is the shocked material
1.0E5   ! initpres is the initial pressure
-0.4   ! xshock is the initial position of shock at t=0
1.0e9   ! pshock

! PROBLEM VARIABLES
2   ! nmat is the total number of materials
1   ! num_particles is number of particles
2   ! nspec is number of species for reaction

! MATERIALS VARIABLES
"stiffeos_Water" ! tag for material 1 (medium)
"stiffeos_Air" ! tag for material 2 (medium)

! REACTION SECTION
17900.0 ! Ea/Ru, K
14.67e6 ! qheat, J/kg
795.0 ! Tswitch, K

! PARTICLE INPUTS
2.0 2.0 2.0 1.0 ! xc0,yc0,zc0,radius
