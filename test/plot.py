import numpy as np 

import matplotlib.pyplot as plt 

for i in [0,1000,2000,3000]:
	state = np.loadtxt('tecplotdatafile_%06d.dat'%i,skiprows=2)
	x = state[:,0]
	rho = state[:,1]
	u = state[:,2]
	p = state[:,3]
	
	plt.subplot(2,3,1)
	plt.plot(x,rho)
	plt.legend(['t = 0', 't= 1', 't = 2', 't = 3'])
	
	plt.subplot(2,3,2)
	plt.plot(x,u)	
	plt.legend(['t = 0', 't= 1', 't = 2', 't = 3'])
	
	plt.subplot(2,3,3)
	plt.plot(x,p)
	plt.legend(['t = 0', 't= 1', 't = 2', 't = 3'])
plt.show()
