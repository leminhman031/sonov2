!***********************************************************************
      subroutine readrocpack
!***********************************************************************

! pull required variables from respective modules
      use globalvar
      use gridvariables
      use Shukla2010

      print*,'Reading in Rocpack file'
      !open (unit=717,file='hmx_slice.ini',action='read')
      !open (unit=717,file='ss.ini',action='read')
      !open (unit=717,file='hmx.ini',action='read')
      !open (unit=717,file='hmx_clean.ini',action='read')
      open (unit=717,file='hmx_fordet03.input',action='read')
      phi=0.0


      do i=1,NI
         read(717,*) ((phi(i,j,k,1),j=1,NJ),k=1,NK)
      enddo
      call BC(2)

      do k=1,NK
      do j=1,NJ
      do i=1,NI
         if (xc(i,j,k) .le. xshock+20*delx) phi(i,j,k,1)=0.0
      enddo
      enddo
      enddo

      do k=1,5
      do j=1,NJ
      do i=1,NI
         if (xc(i,j,k) .le. xshock) phi(i,j,k,1)=0
         if (phi(i,j,k,1).eq.1.and.phi(i+1,j,k,1).eq.0&
            &.and.phi(i+1,j+1,k,1).eq.0.and.phi(i+1,j-1,k,1).eq.0&
            &.and.phi(i-1,j,k,1).eq.0&
            &.and.phi(i-1,j+1,k,1).eq.0.and.phi(i-1,j-1,k,1).eq.0)&
            & phi(i,j,k,1)=0.0
      enddo
      enddo
      enddo

      call updatelastphi
      call BC(2)


      close(717)

      !call BC(2)
      !do j=1,NJ
      !do i=1,NI
      !   if (xc(i,j) .le. xshock+5*delx) phi(i,j,1)=0.0
      !enddo
      !enddo


      print*,'Finished reading in Rocpack file'



      end subroutine readrocpack
!***********************************************************************