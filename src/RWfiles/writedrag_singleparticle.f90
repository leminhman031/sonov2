!***********************************************************************
      subroutine writedrag_singleparticle(currphi)
!***********************************************************************
! this subroutine calculates the drag coefficient for the particle
! and mass averaged particle quantities

      use globalvar
      use gridvariables
      use Shukla2010

      integer, intent(in) :: currphi
! create local variable only used to temporarily hold a value
      real :: fake,drag
      real :: U_mass,P_mass,T_mass,dens1,pmass_TOTAL
      real :: U_mass2,P_mass2,p1mass,xcenter2,P_vol,T_vol,rho_vol,vol2
      real :: x_vol,u_vol
      integer :: kend

#if probdim==3
      kend=NK
#else
      kend=1
#endif



! loop to calculate the dragforce on particle note: PI = 4*atan(1.0)
      drag = 0.0000
      pvol(currphi-nmed) = 0.0000
      pmass(currphi-nmed) = 0.0000
      p1mass = 0.0000
      xcenter = 0.0000
      xcenter2 = 0.0000
      U_mass=0.0000
      U_mass2=0.0000
      P_mass = 0.0000
      P_mass2 = 0.0000
      T_mass = 0.0000
      pmass_TOTAL = 0.0000
      P_vol = 0.00
      x_vol = 0.00
      u_vol = 0.00
      T_vol = 0.00
      vol2 = 0.00
      rho_vol = 0.00

      do k=1,kend
      do j=1,NJ
      do i=1,NI
         fake = 0.5*(p(i+1,j,k)-p(i-1,j,k))/delx


         drag = drag - phi(i,j,k,currphi)*fake*vol(i,j,k)

         pmass_TOTAL = pmass_TOTAL + rho(i,j,k)*vol(i,j,k)

         pvol(currphi-nmed) = pvol(currphi-nmed)&
                  & + phi(i,j,k,currphi)*vol(i,j,k)

         p1mass = p1mass + rhomat(i,j,k,currphi)*vol(i,j,k)
         U_mass2 = U_mass2   + rhomat(i,j,k,currphi)*vol(i,j,k)*u(i,j,k)
         P_mass2 = P_mass2   + rhomat(i,j,k,currphi)*vol(i,j,k)*p(i,j,k)
        xcenter2 = xcenter2 + rhomat(i,j,k,currphi)*vol(i,j,k)*xc(i,j,k)

         if (phi(i,j,k,currphi)>0.999) then
            dens1=rhomat(i,j,k,currphi)/phi(i,j,k,currphi)
            pmass(currphi-nmed) = pmass(currphi-nmed) +dens1*vol(i,j,k)
            xcenter = xcenter + dens1*vol(i,j,k)*xc(i,j,k)
            U_mass = U_mass   + dens1*vol(i,j,k)*u(i,j,k)
            P_mass = P_mass   + dens1*vol(i,j,k)*p(i,j,k)
            T_mass = T_mass   + dens1*vol(i,j,k)*T(i,j,k,currphi)

            vol2 = vol2 + phi(i,j,k,currphi)*vol(i,j,k)
            p_vol = P_vol + phi(i,j,k,currphi)*vol(i,j,k)*p(i,j,k)
            x_vol = x_vol + phi(i,j,k,currphi)*vol(i,j,k)*xc(i,j,k)
            u_vol = u_vol + phi(i,j,k,currphi)*vol(i,j,k)*u(i,j,k)
            t_vol = t_vol + phi(i,j,k,currphi)*&
                     &vol(i,j,k)*T(i,j,k,currphi)
            rho_vol = rho_vol + phi(i,j,k,currphi)*vol(i,j,k)*dens1
         endif

      enddo
      enddo
      enddo

      xcenter = lscale*xcenter/pmass(currphi-nmed)
      xcenter2 = lscale*xcenter2/p1mass

      U_mass  = vscale*U_mass/pmass(currphi-nmed)
      U_mass2  = vscale*U_mass2/p1mass

      P_mass  = pscale*P_mass/pmass(currphi-nmed)
      P_mass2  = pscale*P_mass2/p1mass

      T_mass  = Tempscale*T_mass/pmass(currphi-nmed)


! volume averaged quant:
      rho_vol = rho_vol/vol2
      t_vol = t_vol/vol2
      p_vol = p_vol/vol2
      x_vol = x_vol/vol2
      u_vol = u_vol/vol2


! calculate Cd from dragforce and normalizer
      drag = drag/norml(currphi-nmed)

! calculate t/taus at current timeT (note taus is in seconds)
! taus is calculated in readinput.f90
      fake  = timeT*tscale/taus

! write the calculated drag to the Cd_datafile.txt and timeT
      open(unit=25,file=cdfile,status='old',position='append',&
           action='write')
      write(25,*)fake,pvol(currphi-nmed),p1mass,drag,xcenter,U_mass,P_mass,&
           T_mass,pmass_TOTAL,lamda_vol,lamda_mass,xcenter2,&
           U_mass2,P_mass2,vol2,rho_vol,p_vol,t_vol,x_vol,u_vol
      close(25,status='keep')
! note that xcdiff= xcaftershape - xcafterrho


      end subroutine writedrag_singleparticle
!***********************************************************************
