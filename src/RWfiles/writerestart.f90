!***********************************************************************
      subroutine writerestart()
!***********************************************************************
! this subroutine outputs datafile for restart

      use globalvar
      use gridvariables
      use RungKutt3


      character(len=50) :: fname 

      write(istr,'(I0)') giter
      fname = 'restartfile_'//trim(istr)//'.dat'
      open(unit=70,file=fname,status='new',action='write')

      write(70,*)timeT,giter,deltat

      do k=1,NK
      do j=1,NJ
      do i=1,NI
        write(70,*)rho(i,j,k),u(i,j,k),v(i,j,k),w(i,j,k),p(i,j,k),&
        phi(i,j,k,1:nmat),rhomat(i,j,k,1:nmat),rhoY(i,j,k,1:nspec)
      enddo 
      enddo
      enddo

      close(70,status='keep')
      print*,'restart file written: '//fname//' '
        


      end subroutine writerestart
!***********************************************************************
