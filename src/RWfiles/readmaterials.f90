!***********************************************************************
      subroutine readmaterials
!***********************************************************************
! pull required variables from respective modules
      use globalvar
      use gridvariables
      use Shukla2010
      use mieeos5eqn_ntemp

      integer :: eof



! open the materials list file
      open(UNIT=12,FILE='materials.list',ACTION='READ')

! get material properties for each tag
      do i=1,nmat
        read(12,*) tmpstr

        do while(index(matstr(i),trim(tmpstr))/=1)
          read(12,fmt=*,IOSTAT=eof) tmpstr
          if(eof<0) then 
            write(6,*)'Error: end of file reached, did not find ',&
            &matstr(i)
            close(12)
            stop
          endif
        enddo

        backspace(12)

        if(index(matstr(i),'stiffeos_')==1) then
          read(12,*) tmpstr,eos(i),matprop(2,i),&
                    &matprop(3,i),matprop(4,i)
        elseif(index(matstr(i),'mgeos_')==1) then
          read(12,*) tmpstr,eos(i),matprop(2,i),matprop(4,i),&
                    & matprop(6,i),matprop(7,i),matprop(1,i)
        endif

        rewind(12)
      enddo 

      close(12)




! Fill in rest of matprop array depending on EOS of material
      do i=1,nmat
        if (eos(i) == 1) then ! stiffened eos
        ! Cv J/kg-K
          matprop(1,i) = (initpres+matprop(2,1)*matprop(3,i))&
            /((matprop(2,i)-1.0)*matprop(4,i)*Tempscale) 
        ! e0
          matprop(5,i) = matprop(1,i)*Tempscale 
        ! matprop(6,i) isn't used so set to zero
          matprop(6,i) = 0.00 
        ! speed of sound (m/s)
          matprop(7,i) = sqrt( (matprop(2,i)/matprop(4,i))*&
                   (matprop(3,i)+initpres) ) 

        elseif (eos(i) == 2) then ! Mie-G EOS
        ! add 1 to Gruneisen gamma for fluid mixture eos
          matprop(2,i) = matprop(2,i)+1
        ! e0
          matprop(5,i) = matprop(1,i)*Tempscale
        endif
      enddo 


! turn material prop values into nondimensional values
      do i=1,nmat
        matprop(1,i) = matprop(1,i)/cvscale
        matprop(3,i) = matprop(3,i)/pscale
        matprop(4,i) = matprop(4,i)/rscale
        matprop(5,i) = matprop(5,i)/cvscale/Tempscale
        matprop(7,i) = matprop(7,i)/vscale
      enddo



      end subroutine readmaterials
!***********************************************************************