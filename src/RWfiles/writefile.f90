!***********************************************************************
      subroutine writefile()
!***********************************************************************
! this subroutine outputs datafile in format specified by user in input.txt
! fmtout :
!     0 = matlab
!     1 = tecplot
!     2 = vtk (serial vtk rectilinear grid) 

      use globalvar

      if(fmtout == 0) then
        !call writematlab
      elseif(fmtout==1) then
        call writetecplot
      elseif(fmtout==2) then
        !call writevtk
      endif

      open(unit=25,file=taufile,status='old',position='append',&
           action='write')
      write(25,*)timeT*tscale/taus!(timeT*tscale)
      close(25,status='keep')

      end subroutine writefile
!***********************************************************************
