!***********************************************************************
      subroutine makefiles()
!***********************************************************************
! This subroutine makes output files for write functions 
!     (except writefile() related)

! pull required variables from respective modules
      use globalvar


! SET FILE NAME DEPENDING IF RESTARTFLAG=TRUE
      if( restartflag==1) then
         write(istr,'(I0)') rstiter
         timefile = 'timedatafile_'//trim(istr)//'.dat'
         open(unit=35,file=timefile,status='new',action='write')
         write(35,*)"%iter,phys_time (s),iterphi,iterrho, &
           & resphi,resrho,non_time,deltat,max_sound_speed"
         close(35,status='keep')
         taufile = 'taus_'//trim(istr)//'.dat'
         open(unit=25,file=taufile,status='new',action='write')
         write(25,*)"%t/taus"
         close(25,status='keep')
         tau2file = 'tau2s_'//trim(istr)//'.dat'
         open(unit=25,file=tau2file,status='new',action='write')
         write(25,*)"%t/taus"
         close(25,status='keep')
         cdfile = 'CD'//trim(istr)//'.dat'
         open(unit=25,file=cdfile,status='new',action='write')
         write(25,*)"%t/taus,pvol,pmass,cd,xcenter,u_mass,p_mass,T_mass"
         close(25,status='keep')

      else
         timefile = 'timedatafile.dat'
         open(unit=35,file=timefile,status='new',action='write')
         write(35,*)"%iter,phys_time (s),iterphi,iterrho, &
           & resphi,resrho,non_time,deltat,max_sound_speed"
         close(35,status='keep')

         cdfile = 'CD.dat'
         open(unit=25,file=cdfile,status='new',action='write')
         write(25,*)"%t/taus,pvol,pmass,cd,xcenter,u_mass,p_mass,T_mass"
         close(25,status='keep')

         taufile = 'taus.dat'
         open(unit=25,file=taufile,status='new',action='write')
         write(25,*)"%t/taus"
         close(25,status='keep')



      endif

      


      end subroutine makefiles
!***********************************************************************
