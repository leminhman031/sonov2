!***********************************************************************
      subroutine BC(sel)
!***********************************************************************
! This subroutine applies boundary conditions


! pull required variables from respective modules
      use globalvar
      use gridvariables
      use RungKutt3, only:rhoY


! display intent of input variables
      integer,  intent(in) :: sel


! ----------------------------------------------------------------------
!     sel=1 General Boundary Condition
! ----------------------------------------------------------------------
      if(sel==1) then
#if PERBC==0
#if GEOMETRY==2
! Axisym BC
! DO BC for bottom and top walls
      do k=1,NK
      do j=(-NG+1),0
      do i=1,NI
! BC for bottom wall (symmetry BC is used)
         rho(i,j,k) = rho(i,-1*j+1,k)
         u(i,j,k) = u(i,-1*j+1,k)
         v(i,j,k) = -1.0*v(i,-1*j+1,k)
         w(i,j,k) = w(i,-1*j+1,k)
         p(i,j,k) = p(i,-1*j+1,k)
         T(i,j,k,0) = T(i,-1*j+1,k,0)
! BC top wall is transmitive
         rho(i,j+NJ+NG,k) = rho(i,NJ,k)
         u(i,j+NJ+NG,k) = u(i,NJ,k)
         v(i,j+NJ+NG,k) = v(i,NJ,k)
         w(i,j+NJ+NG,k) = w(i,NJ,k)
         p(i,j+NJ+NG,k) = p(i,NJ,k)
         T(i,j+NJ+NG,k,0) = T(i,NJ,k,0)
      enddo
      enddo
      enddo

! DO BC for bottom and top walls
      do loop=1,nmat
      do k=1,NK
      do j=(-NG+1),0
      do i=1,NI
! BC for bottom wall (symmetry BC is used)
         phi(i,j,k,loop) = phi(i,-1*j+1,k,loop)
         rhomat(i,j,k,loop) = rhomat(i,-1*j+1,k,loop)
! BC top wall is transmitive
         phi(i,j+NJ+NG,k,loop) = phi(i,NJ,k,loop)
         rhomat(i,j+NJ+NG,k,loop) = rhomat(i,NJ,k,loop)
      enddo
      enddo
      enddo
      enddo 
#if  REACT==1 
! DO BC for bottom and top walls
      do loop=1,nspec
      do k=1,NK
      do j=(-NG+1),0
      do i=1,NI
! BC for bottom wall (symmetry BC is used)
         rhoY(i,j,k,loop) = rhoY(i,-1*j+1,k,loop)
! BC top wall is transmitive
         rhoY(i,j+NJ+NG,k,loop) = rhoY(i,NJ,k,loop)
      enddo
      enddo
      enddo
      enddo
#endif

#else  
! 2D Cartesian Transmittive BC on top and bottom wall

! DO BC for bottom and top walls
      do k=1,NK
      do j=(-NG+1),0
      do i=1,NI
! BC for bottom wall (transmittive BC is used)
         rho(i,j,k) = rho(i,1,k)
         u(i,j,k) = u(i,1,k)
         v(i,j,k) = v(i,1,k)
         w(i,j,k) = w(i,1,k)
         p(i,j,k) = p(i,1,k)
         T(i,j,k,0) = T(i,1,k,0)
! BC top wall is transmitive
         rho(i,j+NJ+NG,k) = rho(i,NJ,k)
         u(i,j+NJ+NG,k) = u(i,NJ,k)
         v(i,j+NJ+NG,k) = v(i,NJ,k)
         w(i,j+NJ+NG,k) = w(i,NJ,k)
         p(i,j+NJ+NG,k) = p(i,NJ,k)
         T(i,j+NJ+NG,k,0) = T(i,NJ,k,0)
      enddo
      enddo
      enddo

! DO BC for bottom and top walls
      do loop=1,nmat
      do k=1,NK
      do j=(-NG+1),0
      do i=1,NI
! BC for bottom wall (transmittive BC is used)
         phi(i,j,k,loop) = phi(i,1,k,loop)
         rhomat(i,j,k,loop) = rhomat(i,1,k,loop)
! BC top wall is transmitive
         phi(i,j+NJ+NG,k,loop) = phi(i,NJ,k,loop)
         rhomat(i,j+NJ+NG,k,loop) = rhomat(i,NJ,k,loop)
      enddo
      enddo
      enddo
      enddo

#if  REACT==1 
! DO BC for bottom and top walls
      do loop=1,nspec
      do k=1,NK
      do j=(-NG+1),0
      do i=1,NI
! BC for bottom wall (transmittive BC is used)
         rhoY(i,j,k,loop) = rhoY(i,1,k,loop)
! BC top wall is transmitive
         rhoY(i,j+NJ+NG,k,loop) = rhoY(i,NJ,k,loop)
      enddo
      enddo
      enddo
      enddo
#endif


#endif

!***********************************************************
! this section does periodic BC for top/bot wall
#else
! DO BC for bottom wall
      do k=1,NK
      do j=(-NG+1),0
      do i=1,NI
         rho(i,j,k) = rho(i,NJ+j,k)
         u(i,j,k) = u(i,NJ+j,k)
         v(i,j,k) = v(i,NJ+j,k)
         w(i,j,k) = w(i,NJ+j,k)
         p(i,j,k) = p(i,NJ+j,k)
         T(i,j,k,0) = T(i,NJ+j,k,0)
      enddo
      enddo
      enddo
! DO BC for bottom wall
      do loop=1,nmat
      do k=1,NK
      do j=(-NG+1),0
      do i=1,NI
         phi(i,j,k,loop) = phi(i,NJ+j,k,loop)
         rhomat(i,j,k,loop) = rhomat(i,NJ+j,k,loop)
      enddo
      enddo
      enddo
      enddo

#if  REACT==1 
! DO BC for bottom wall
      do loop=1,nspec
      do k=1,NK
      do j=(-NG+1),0
      do i=1,NI
         rhoY(i,j,k,loop) = rhoY(i,NJ+j,k,loop)
      enddo
      enddo
      enddo
      enddo
#endif


! DO BC for top wall
      do k=1,NK
      do j=1,NG
      do i=1,NI
         rho(i,NJ+j,k) = rho(i,j,k)
         u(i,NJ+j,k) = u(i,j,k)
         v(i,NJ+j,k) = v(i,j,k)
         w(i,NJ+j,k) = w(i,j,k)
         p(i,NJ+j,k) = p(i,j,k)
         T(i,NJ+j,k,0) = T(i,j,k,0)
      enddo
      enddo
      enddo
! DO BC for top wall
      do loop=1,nmat
      do k=1,NK
      do j=1,NG
      do i=1,NI
         phi(i,NJ+j,k,loop) = phi(i,j,k,loop)
         rhomat(i,NJ+j,k,loop) = rhomat(i,j,k,loop)
      enddo
      enddo
      enddo
      enddo


#if  REACT==1 
! DO BC for top wall
      do loop=1,nspec
      do k=1,NK
      do j=1,NG
      do i=1,NI
         rhoY(i,NJ+j,k,loop) = rhoY(i,j,k,loop)
      enddo
      enddo
      enddo
      enddo
#endif


#endif
!***********************************************************


! DO BC for inlet and outlet
      do k=1,NK
      do j=(-NG+1),(NJ+NG)
      do i=(-NG+1),0
! First is inlet (x=0 plane is inlet BC)
#if probdim==1
! have a 1D problem
#if GEOMETRY==1
!  1D problem in cartesian geometry, use outflowBC at x=0 
         rho(i,j,k) = rho(1,j,k)
         u(i,j,k) = u(1,j,k)
         p(i,j,k) = p(1,j,k)
         T(i,j,k,0) = T(1,j,k,0)
#else
! 1D problem in cylindrical or spherical geometry, use symmetry at x=0 
         rho(i,j,k) = rho(-1*i+1,j,k)
         u(i,j,k) = -1.0*u(-1*i+1,j,k)
         p(i,j,k) = p(-1*i+1,j,k)
         T(i,j,k,0) = T(-1*i+1,j,k,0)
#endif
#else
! 2D/3D problem, use transmissive BC for all coordinate systems
         rho(i,j,k) = rho(1,j,k)
         u(i,j,k) = u(1,j,k)
         v(i,j,k) = v(1,j,k)
         w(i,j,k) = w(1,j,k)
         p(i,j,k) = p(1,j,k)
         T(i,j,k,0) = T(1,j,k,0)
#endif
! Second is outlet
         rho(i+NI+NG,j,k) = rho(NI,j,k)
         u(i+NI+NG,j,k) = u(NI,j,k)
         v(i+NI+NG,j,k) = v(NI,j,k)
         w(i+NI+NG,j,k) = w(NI,j,k)
         T(i+NI+NG,j,k,0) = T(NI,j,k,0)
         p(i+NI+NG,j,k) = p(NI,j,k)
      enddo
      enddo 
      enddo 

! DO BC for inlet and outlet
      do loop=1,nmat
      do k=1,NK
      do j=(-NG+1),(NJ+NG)
      do i=(-NG+1),0
! First is inlet (x=0 plane is inlet BC)
#if probdim==1
! have a 1D problem
#if GEOMETRY==1
!  1D problem in cartesian geometry, use outflowBC at x=0 
         phi(i,j,k,loop) = phi(1,j,k,loop)
         rhomat(i,j,k,loop) = rhomat(1,j,k,loop)
#else
! 1D problem in cylindrical or spherical geometry, use symmetry at x=0 
         phi(i,j,k,loop) = phi(-1*i+1,j,k,loop)
         rhomat(i,j,k,loop) = rhomat(-1*i+1,j,k,loop)
#endif

#else
! 2D/3D problem, use transmissive BC for all coordinate systems
         phi(i,j,k,loop) = phi(1,j,k,loop)
         rhomat(i,j,k,loop) = rhomat(1,j,k,loop)
#endif

! Second is outlet
         phi(i+NI+NG,j,k,loop) = phi(NI,j,k,loop)
         rhomat(i+NI+NG,j,k,loop) = rhomat(NI,j,k,loop)
      enddo
      enddo
      enddo
      enddo 

#if  REACT==1 
! DO BC for inlet and outlet
      do loop=1,nspec
      do k=1,NK
      do j=(-NG+1),(NJ+NG)
      do i=(-NG+1),0
! First is inlet (x=0 plane is inlet BC)
#if probdim==1
! have a 1D problem
#if GEOMETRY==1
!  1D problem in cartesian geometry, use outflowBC at x=0 
         rhoY(i,j,k,loop) = rhoY(1,j,k,loop)
#else
! 1D problem in cylindrical or spherical geometry, use symmetry at x=0 
         rhoY(i,j,k,loop) = rhoY(-1*i+1,j,k,loop)
#endif

#else
! 2D/3D problem, use transmissive BC for all coordinate systems
         rhoY(i,j,k,loop) = rhoY(1,j,k,loop)
#endif
! Second is outlet
         rhoY(i+NI+NG,j,k,loop) = rhoY(NI,j,k,loop)
      enddo
      enddo
      enddo
      enddo 
#endif




#if probdim==3
! Do BC for ghost cells in depth direction (front/back)
      do k=(-NG+1),0
      do j=(-NG+1),(NJ+NG)
      do i=(-NG+1),(NI+NG)
! First is front
         rho(i,j,k) = rho(i,j,1)
         u(i,j,k) = u(i,j,1)
         v(i,j,k) = v(i,j,1)
         w(i,j,k) = w(i,j,1)
         p(i,j,k) = p(i,j,1)
         T(i,j,k,0) = T(i,j,1,0)
! Second is back
         rho(i,j,k+NK+NG) = rho(i,j,NK)
         u(i,j,k+NK+NG) = u(i,j,NK)
         v(i,j,k+NK+NG) = v(i,j,NK)
         w(i,j,k+NK+NG) = w(i,j,NK)
         T(i,j,k+NK+NG,0) = T(i,j,NK,0)
         p(i,j,k+NK+NG) = p(i,j,NK)
      enddo
      enddo 
      enddo 

! Do BC for ghost cells in depth direction (front/back)
      do loop=1,nmat
      do k=(-NG+1),0
      do j=(-NG+1),(NJ+NG)
      do i=(-NG+1),(NI+NG)
! First is front
         phi(i,j,k,loop) = phi(i,j,1,loop)
         rhomat(i,j,k,loop) = rhomat(i,j,1,loop)
! Second is back
         phi(i,j,k+NK+NG,loop) = phi(i,j,NK,loop)
         rhomat(i,j,k+NK+NG,loop) = rhomat(i,j,NK,loop)
      enddo
      enddo 
      enddo
      enddo

#if  REACT==1 
! Do BC for ghost cells in depth direction (front/back)
      do loop=1,nspec
      do k=(-NG+1),0
      do j=(-NG+1),(NJ+NG)
      do i=(-NG+1),(NI+NG)
! First is front
         rhoY(i,j,k,loop) = rhoY(i,j,1,loop)
! Second is back
         rhoY(i,j,k+NK+NG,loop) = rhoY(i,j,NK,loop)
      enddo
      enddo 
      enddo
      enddo
#endif

#endif

      endif


! ----------------------------------------------------------------------
!     sel=2 Boundary Condition for Shukla2010phi correction
! ----------------------------------------------------------------------
      if(sel==2) then
! for phi correction it uses transmissive BC always
#if PERBC==0
#if GEOMETRY==2
! DO BC for bottom and top walls
      do loop=1,nmat
      do k=1,NK
      do j=(-NG+1),0
      do i=1,NI
! BC for bottom wall (symmetry BC is used)
         phi(i,j,k,loop) = phi(i,-1*j+1,k,loop)
! BC top wall is transmitive
         phi(i,j+NJ+NG,k,loop) = phi(i,NJ,k,loop)
      enddo
      enddo
      enddo
      enddo 
 
#else  
! 2D Cartesian BC 
! DO BC for bottom and top walls
      do loop=1,nmat
      do k=1,NK
      do j=(-NG+1),0
      do i=1,NI
! BC for bottom wall (transmittive BC is used)
         phi(i,j,k,loop) = phi(i,1,k,loop)
! BC top wall is transmitive
         phi(i,j+NJ+NG,k,loop) = phi(i,NJ,k,loop)
      enddo
      enddo
      enddo
      enddo
#endif

!***********************************************************
! this section does periodic BC for top/bot wall
#else
! DO BC for bottom wall
      do loop=1,nmat
      do k=1,NK
      do j=(-NG+1),0
      do i=1,NI
         phi(i,j,k,loop) = phi(i,NJ+j,k,loop)
      enddo
      enddo
      enddo
      enddo
! DO BC for top wall
      do loop=1,nmat
      do k=1,NK
      do j=1,NG
      do i=1,NI
         phi(i,NJ+j,k,loop) = phi(i,j,k,loop)
      enddo
      enddo
      enddo
      enddo
#endif
!***********************************************************

! DO BC for inlet and outlet
      do loop=1,nmat
      do k=1,NK
      do j=(-NG+1),(NJ+NG)
      do i=(-NG+1),0
! First is inlet
         phi(i,j,k,loop) = phi(1,j,k,loop)
! Second is outlet
         phi(i+NI+NG,j,k,loop) = phi(NI,j,k,loop)
      enddo
      enddo
      enddo
      enddo 

#if probdim==3
! Do BC for ghost cells in depth direction (front/back)
      do loop=1,nmat
      do k=(-NG+1),0
      do j=(-NG+1),(NJ+NG)
      do i=(-NG+1),(NI+NG)
! First is front
         phi(i,j,k,loop) = phi(i,j,1,loop)
! Second is back
         phi(i,j,k+NK+NG,loop) = phi(i,j,NK,loop)
      enddo
      enddo 
      enddo
      enddo
#endif

      endif  !endif for sel==2


! ----------------------------------------------------------------------
!     sel=3 Boundary Condition for Shukla2010rho correction (4 eqn)
! ----------------------------------------------------------------------
      if(sel==3) then
#if PERBC==0
#if GEOMETRY==2 
! Axisym BC 
! DO BC for bottom and top walls
      do k=1,NK
      do j=(-NG+1),0
      do i=1,NI
! BC for bottom wall (symmetry BC is used)
         rho(i,j,k) = rho(i,-1*j+1,k)
! BC top wall is transmitive
         rho(i,j+NJ+NG,k) = rho(i,NJ,k)
      enddo
      enddo
      enddo

#else 
! 2D Cartesian BC
! DO BC for bottom and top walls
      do k=1,NK
      do j=(-NG+1),0
      do i=1,NI
! BC for bottom wall (transmittive BC is used)
         rho(i,j,k) = rho(i,1,k)
! BC top wall is transmitive
         rho(i,j+NJ+NG,k) = rho(i,NJ,k)
      enddo
      enddo
      enddo

#endif 


!***********************************************************
! this section does periodic BC for top/bot wall
#else
! DO BC for bottom wall
      do k=1,NK
      do j=(-NG+1),0
      do i=1,NI
         rho(i,j,k) = rho(i,NJ+j,k)
      enddo
      enddo
      enddo
! DO BC for top wall
      do k=1,NK
      do j=1,NG
      do i=1,NI
         rho(i,NJ+j,k) = rho(i,j,k)
      enddo
      enddo
      enddo
#endif
!***********************************************************

! DO BC for inlet and outlet
      do k=1,NK
      do j=(-NG+1),(NJ+NG)
      do i=(-NG+1),0
! First is inlet (x=0 plane is inlet BC)
#if probdim==1
! have a 1D problem
#if GEOMETRY==1
!  1D problem in cartesian geometry, use outflowBC at x=0 
         rho(i,j,k) = rho(1,j,k)
#else
! 1D problem in cylindrical or spherical geometry, use symmetry at x=0 
         rho(i,j,k) = rho(-1*i+1,j,k)
#endif

#else
! 2D/3D problem, use transmissive BC for all coordinate systems
         rho(i,j,k) = rho(1,j,k)
#endif
! Second is outlet
         rho(i+NI+NG,j,k) = rho(NI,j,k)
      enddo
      enddo 
      enddo 



#if probdim==3
! Do BC for ghost cells in depth direction (front/back)
      do k=(-NG+1),0
      do j=(-NG+1),(NJ+NG)
      do i=(-NG+1),(NI+NG)
! First is front
         rho(i,j,k) = rho(i,j,1)
! Second is back
         rho(i,j,k+NK+NG) = rho(i,j,NK)
      enddo
      enddo 
      enddo 
#endif

      endif  !endif for sel==3


! ----------------------------------------------------------------------
!     sel=4 Boundary Condition for Shukla2010rho correction (5 eqn)
! ----------------------------------------------------------------------
      if(sel==4) then


#if PERBC==0
#if GEOMETRY==2
! Axisym BC 
! DO BC for bottom and top walls
      do loop=1,nmat
      do k=1,NK
      do j=(-NG+1),0
      do i=1,NI
! BC for bottom wall (symmetry BC is used)
         rhomat(i,j,k,loop) = rhomat(i,-1*j+1,k,loop)
! BC top wall is transmitive
         rhomat(i,j+NJ+NG,k,loop) = rhomat(i,NJ,k,loop)
      enddo
      enddo
      enddo
      enddo 

#else 
! 2D Cartesian BC
! DO BC for bottom and top walls
      do loop=1,nmat
      do k=1,NK
      do j=(-NG+1),0
      do i=1,NI
! BC for bottom wall (transmittive BC is used)
         rhomat(i,j,k,loop) = rhomat(i,1,k,loop)
! BC top wall is transmitive
         rhomat(i,j+NJ+NG,k,loop) = rhomat(i,NJ,k,loop)
      enddo
      enddo
      enddo
      enddo
#endif 


!***********************************************************
! this section does periodic BC for top/bot wall
#else
! DO BC for bottom wall
      do loop=1,nmat
      do k=1,NK
      do j=(-NG+1),0
      do i=1,NI
         rhomat(i,j,k,loop) = rhomat(i,NJ+j,k,loop)
      enddo
      enddo
      enddo
      enddo
! DO BC for top wall
      do loop=1,nmat
      do k=1,NK
      do j=1,NG
      do i=1,NI
         rhomat(i,NJ+j,k,loop) = rhomat(i,j,k,loop)
      enddo
      enddo
      enddo
      enddo
#endif
!***********************************************************

! DO BC for inlet and outlet
      do loop=1,nmat
      do k=1,NK
      do j=(-NG+1),(NJ+NG)
      do i=(-NG+1),0
! First is inlet (x=0 plane is inlet BC)
#if probdim==1
! have a 1D problem
#if GEOMETRY==1
!  1D problem in cartesian geometry, use outflowBC at x=0 
         rhomat(i,j,k,loop) = rhomat(1,j,k,loop)
#else
! 1D problem in cylindrical or spherical geometry, use symmetry at x=0
         rhomat(i,j,k,loop) = rhomat(-1*i+1,j,k,loop)
#endif
#else
! 2D/3D problem, use transmissive BC for all coordinate systems
         rhomat(i,j,k,loop) = rhomat(1,j,k,loop)
#endif
! Second is outlet
         rhomat(i+NI+NG,j,k,loop) = rhomat(NI,j,k,loop)
      enddo
      enddo
      enddo
      enddo 


#if probdim==3
! Do BC for ghost cells in depth direction (front/back)
      do loop=1,nmat
      do k=(-NG+1),0
      do j=(-NG+1),(NJ+NG)
      do i=(-NG+1),(NI+NG)
! First is front
         rhomat(i,j,k,loop) = rhomat(i,j,1,loop)
! Second is back
         rhomat(i,j,k+NK+NG,loop) = rhomat(i,j,NK,loop)
      enddo
      enddo 
      enddo
      enddo
#endif

      endif !endif for sel==4


! ----------------------------------------------------------------------
!     sel=5 Boundary Condition for TEMP correction 
! ----------------------------------------------------------------------
      if(sel==5) then

#if PERBC==0
#if GEOMETRY==2
! Axisym BC 
! DO BC for bottom and top walls
      do k=1,NK
      do j=(-NG+1),0
      do i=1,NI
! BC for bottom wall (symmetry BC is used)
         T(i,j,k,0) = T(i,-1*j+1,k,0)
! BC top wall is transmitive
         T(i,j+NJ+NG,k,0) = T(i,NJ,k,0)
      enddo
      enddo
      enddo

#else 
! 2D Cartesian BC
! DO BC for bottom and top walls
      do k=1,NK
      do j=(-NG+1),0
      do i=1,NI
! BC for bottom wall (transmittive BC is used)
         T(i,j,k,0) = T(i,1,k,0)
! BC top wall is transmitive
         T(i,j+NJ+NG,k,0) = T(i,NJ,k,0)
      enddo
      enddo
      enddo

#endif 


!***********************************************************
! this section does periodic BC for top/bot wall
#else
! DO BC for bottom wall
      do k=1,NK
      do j=(-NG+1),0
      do i=1,NI
         T(i,j,k,0) = T(i,NJ+j,k,0)
      enddo
      enddo
      enddo
! DO BC for top wall
      do k=1,NK
      do j=1,NG
      do i=1,NI
         T(i,NJ+j,k,0) = T(i,j,k,0)
      enddo
      enddo
      enddo
#endif
!***********************************************************

! DO BC for inlet and outlet
      do k=1,NK
      do j=(-NG+1),(NJ+NG)
      do i=(-NG+1),0
! First is inlet (x=0 plane is inlet BC)
#if probdim==1
! have a 1D problem
#if GEOMETRY==1
!  1D problem in cartesian geometry, use outflowBC at x=0
         T(i,j,k,0) = T(1,j,k,0)
#else
! 1D problem in cylindrical or spherical geometry, use symmetry at x=0
         T(i,j,k,0) = T(-1*i+1,j,k,0)
#endif
#else
! 2D/3D problem, use transmissive BC for all coordinate systems
         T(i,j,k,0) = T(1,j,k,0)
#endif
! Second is outlet
         T(i+NI+NG,j,k,0) = T(NI,j,k,0)
      enddo
      enddo 
      enddo 


#if probdim==3
! Do BC for ghost cells in depth direction (front/back)
      do k=(-NG+1),0
      do j=(-NG+1),(NJ+NG)
      do i=(-NG+1),(NI+NG)
! First is front
         T(i,j,k,0) = T(i,j,1,0)
! Second is back
         T(i,j,k+NK+NG,0) = T(i,j,NK,0)
      enddo
      enddo 
      enddo 
#endif

      endif  !endif for sel==5


      end subroutine BC
!***********************************************************************
