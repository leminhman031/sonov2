!***********************************************************************
      subroutine updatereaction()
!***********************************************************************
! This subroutine updates netflux array with reaction terms if REACT==1

! pull required variables from respective modules
      use globalvar
      use gridvariables
      use Shukla2010
      use TVDfunc, only: netFlux
      use RungKutt3
      use mieeos5eqn_ntemp

      real :: k1_react,ymass,tempjj,k2_react,nu1,nu2,rn
      real :: tophat,q1

      T_ignition = 868.0/Tempscale

      do loop=1,nspec
      do k=1,NK
      do j=1,NJ
      do i=1,NI


         ! microscale reaction rate

         A_hs = 2.0e17*tscale/pscale
         del = 10.0e-6/lscale
         ta = 4.0e-9/tscale
         ta = 0.0e-9/tscale
         tb = 1.5
         del_hs = 20.0e9*tscale

         us = u_postshock/vscale
         r2 = (xc(i,j,k)-us*timeT)**2! + yc(i,j)*yc(i,j)
         tophat = 0.5*(tanh(del_hs*(timeT-ta))-tanh(del_hs*(timeT-tb)))
         dist = exp(-0.5*r2/del/del)

         w_micro = A_hs*tophat*dist
         w_micro = 0.0 ! set to zero for pulse

         ! macroscale reaction rate

         ymass = rhoY(i,j,k,loop)/rho(i,j,k) 
         if (ymass.ge.1.0) ymass=1.0
         if (ymass.le.0.0) ymass=0.0

         ! growth; q1
         q1 = 1.4e9*tscale ! ~ 193.2
         wg = 0.0
         if (ymass.le.0.99999) then
            wg = q1*ymass*(1.0-ymass)*(rho(i,j,k)*rho(i,j,k))
         endif

         ! ignition
         rn = 4.0
         k1_react = 2.3e11*tscale ! ~ 31740.
         wi = 0.0
         if (T(i,j,k,0).ge.T_ignition) then
            wi = k1_react*ymass*((1.0-rho(i,j,k))**rn)
         endif

         ! Arrhenius kinetics
         !wi = (tswitch**2)*ymass*&
         !    &exp((theta/tswitch)-(theta/T(i,j,k,0)))/theta
         !wi = Da*ymass*exp(-theta/T(i,j,k,0))

         w_macro = (wi + wg)*rho(i,j,k)
       
         netflux(i,j,k,5) = netflux(i,j,k,5) + beta*w_macro + w_micro
 
         lp1=6+nmat+nmat
         netflux(i,j,k,lp1+loop) = netflux(i,j,k,lp1+loop) - w_macro

      enddo 
      enddo 
      enddo 
      enddo 


      end subroutine updatereaction
!***********************************************************************
