!***********************************************************************
      subroutine updatelastphi
!***********************************************************************

! pull required variables from respective modules
      use globalvar
      use gridvariables
      use Shukla2010


       do k=1,NK
       do j=1,NJ
       do i=1,NI
        tmp = 0.0000
        if(nmed.gt.1)then
          do lp = 1, (nmed-1)
             tmp = tmp + phi(i,j,k,lp)
          enddo
        endif
        if(num_particles.gt.0)then
          do lp = 1,num_particles
             tmp = tmp + phi(i,j,k,nmed+lp)
          enddo
        endif
         phi(i,j,k,nmat-num_particles) = 1.000-tmp
       enddo
       enddo
       enddo

! apply boundary condition  
      call BC(2) 


      end subroutine updatelastphi
!***********************************************************************
