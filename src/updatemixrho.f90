!***********************************************************************
      subroutine updatemixrho
!***********************************************************************
! Note 5-equation model is implicitly assumed on
! Note rhomat = rho*phi is implicitly assumed on
! pull required variables from respective modules
      use globalvar
      use gridvariables
      use Shukla2010

       do k=1,NK
       do j=1,NJ
       do i=1,NI
        tmp = 0.0000
          do lp = 1, nmat
             tmp = tmp + rhomat(i,j,k,lp)
          enddo
        rho(i,j,k)=tmp
       enddo
       enddo
       enddo



      end subroutine updatemixrho
!***********************************************************************
