!***********************************************************************
      subroutine calcrhorhs5(currphi)
!***********************************************************************

! pull required variables from respective modules
      use globalvar
      use gridvariables
      use Shukla2010
      use RungKutt3, only: temp

      integer, intent(in) :: currphi 

      psi = 0.00
      netrho = 0.00
      tmprho = 0.00

 
      call BC(1)

! ----------------------------------------------------------------------
!     Calculate Psi with knowing Phi for (nmat) Phi
! ----------------------------------------------------------------------
! This is for (nmat) phi
      do k=(-NG+1),NK+NG
      do j=(-NG+1),NJ+NG
      do i=(-NG+1),NI+NG
! reset temp array to zero for array parts that correspond to netrho
        temp(i,j,k,currphi+nmat) = 0.000
        tmp = abs(phi(i,j,k,currphi))**alpha
        psi(i,j,k,currphi) = tmp/&
                &(tmp + abs(1-phi(i,j,k,currphi))**alpha)
      enddo !i=(-NG+1),NI+NG
      enddo !j=(-NG+1),NJ+NG
      enddo !k=(-NG+1),NK+NG


      call BC(4)

! ----------------------------------------------------------------------
!      Axial Direction 
! ----------------------------------------------------------------------
      do k=1,NK
      do j=1,NJ
      do i=1,NI+1

! 1st need to calculate Phi, its derivatives, and normal vector 
!     at left cell face
! phi at left cell face is calculated from Psi at left cell face
! phim is phi at left cell face, and psim is psi at left cell face
         psim = 0.5*(psi(i-1,j,k,currphi)+psi(i,j,k,currphi))
         tmp = abs(psim)**(1.0/alpha)
         phim = tmp/(tmp + abs(1-psim)**(1.0/alpha))

! calculate psi derivatives at left cell face from Shukla 2010 
!     paper page 7419
! these will be used to calculate phim derivatives at left cell face
          psix = (psi(i,j,k,currphi) - psi(i-1,j,k,currphi))/delx
          psiy = 0.00
          psiz = 0.00
#if probdim==2
          psiy = 0.25*(psi(i,j+1,k,currphi) - psi(i,j-1,k,currphi) + &
               & psi(i-1,j+1,k,currphi) - psi(i-1,j-1,k,currphi))/dely
#elif probdim==3
          psiy = 0.25*(psi(i,j+1,k,currphi) - psi(i,j-1,k,currphi) + &
               & psi(i-1,j+1,k,currphi) - psi(i-1,j-1,k,currphi))/dely

          psiz = 0.25*(psi(i-1,j,k+1,currphi) - psi(i-1,j,k-1,currphi)+&
                     & psi(i,j,k+1,currphi) - psi(i,j,k-1,currphi))/delz
#endif

        tmp = abs(phim*(1.0-phim))**(1.0-alpha)
        tmp1 = abs(phim)**alpha + abs(1.0-phim)**alpha
        tmp2 = tmp*(tmp1*tmp1)

! phix and phiy are x and y derivatives of phi 
        phix = tmp2*psix/alpha
        phiy = tmp2*psiy/alpha
        phiz = tmp2*psiz/alpha 

! magnitude of gradient of phi
        tmp = sqrt(phix**2 + phiy**2+phiz**2 + eps*eps)

! calculate normal vector 
        nx = phix/tmp
        ny = phiy/tmp
        nz = phiz/tmp

! calculate drhodx and drhody from Shukla 2010 
!     paper page 7419
        rho_x = (rhomat(i,j,k,currphi) - rhomat(i-1,j,k,currphi))/delx
        rho_y = 0.00
        rho_z = 0.00
#if probdim==2
        rho_y = 0.25*(rhomat(i,j+1,k,currphi)&
                  & - rhomat(i,j-1,k,currphi) +  &
                    & rhomat(i-1,j+1,k,currphi)&
                    & - rhomat(i-1,j-1,k,currphi))/dely
#elif probdim==3
        rho_y = 0.25*(rhomat(i,j+1,k,currphi)&
                  & - rhomat(i,j-1,k,currphi) +  &
                    & rhomat(i-1,j+1,k,currphi)&
                  & - rhomat(i-1,j-1,k,currphi))/dely

        rho_z = 0.25*(rhomat(i-1,j,k+1,currphi)&
                  & - rhomat(i-1,j,k-1,currphi) + &
                    & rhomat(i,j,k+1,currphi)&
                  & - rhomat(i,j,k-1,currphi))/delz
#endif

! calculate fbar from Shukla 2010 paper page 7419
        tmp2 = lsrho*(nx*rho_x + ny*rho_y + nz*rho_z)

! calculate first term of rhs density correction equation 23 
!     from Shukla 2010 paper page 7419
         rhom = 0.5*(rhomat(i-1,j,k,currphi)+rhomat(i,j,k,currphi))

         if(i>1) then
          netrho(i-1,j,k,currphi) = netrho(i-1,j,k,currphi) - rhom/delx
          tmprho(i-1,j,k,currphi) = tmprho(i-1,j,k,currphi) + tmp2/delx
         endif

         if(i<NI+1) then
          netrho(i,j,k,currphi) = netrho(i,j,k,currphi) + rhom/delx  
          tmprho(i,j,k,currphi) = tmprho(i,j,k,currphi) - tmp2/delx 
         endif

         if(i>1) then
          temp(i-1,j,k,currphi+nmat) = (1.0-2.0*phi(i-1,j,k,currphi))*&
                 rhonxcent(i-1,j,k,currphi)*netrho(i-1,j,k,currphi) +&
                 rhonxcent(i-1,j,k,currphi)*tmprho(i-1,j,k,currphi)
         endif

      enddo !i=1,NI+1
      enddo !j=1,NJ
      enddo !k=1,NK
      !enddo !currphi=1,nmat

      netrho = 0.00
      tmprho = 0.00
#if probdim==1
      goto 112
#endif

! ----------------------------------------------------------------------
!     loop through TRANSVERSE DIRECTION
! ----------------------------------------------------------------------
      do k=1,NK
      do j=1,NJ+1
      do i=1,NI

! 1st need to calculate Phi, its derivatives, and normal vector 
!     at bot cell face
! phi at bot cell face is calculated from Psi at bot cell face
! phim is phi at bot cell face, and psim is psi at bot cell face
         psim = 0.5*(psi(i,j-1,k,currphi)+psi(i,j,k,currphi))
         tmp = abs(psim)**(1.0/alpha)
         phim = tmp/(tmp + abs(1-psim)**(1.0/alpha))

! calculate psi derivatives at bot cell face from Shukla 2010 
!     paper page 7419
! these will be used to calculate phim derivatives at bot cell face
        psix = 0.25*(psi(i+1,j,k,currphi) - psi(i-1,j,k,currphi) + &
               & psi(i+1,j-1,k,currphi) - psi(i-1,j-1,k,currphi))/delx

        psiy = (psi(i,j,k,currphi) - psi(i,j-1,k,currphi))/dely
        psiz = 0.00

#if probdim==3
        psiz = 0.25*(psi(i,j-1,k+1,currphi) - psi(i,j-1,k-1,currphi) + &
                   & psi(i,j,k+1,currphi) - psi(i,j,k-1,currphi))/delz
#else
        psiz = 0.00
#endif

        tmp = abs(phim*(1.0-phim))**(1.0-alpha)
        tmp1 = abs(phim)**alpha + abs(1.0-phim)**alpha
        tmp2 = tmp*(tmp1*tmp1)

! phix and phiy are x and y derivatives of phi
        phix = tmp2*psix/alpha 
        phiy = tmp2*psiy/alpha
        phiz = tmp2*psiz/alpha  

! magnitude of gradient of phi 
        tmp = sqrt(phix**2 + phiy**2 +phiz**2+ eps*eps)

        if( tmp==0.0) then
         print*,"trans direction: ",i,j,phix,phiy,eps,tmp
         stop
        endif

! calculate normal vector 
        nx = phix/tmp
        ny = phiy/tmp
        nz = phiz/tmp

! calculate drhodx and drhody from Shukla 2010 
!     paper page 7419
        rho_z = 0.00
        rho_y = (rhomat(i,j,k,currphi) - rhomat(i,j-1,k,currphi))/dely
        rho_x = 0.25*(rhomat(i+1,j,k,currphi)&
                  & - rhomat(i-1,j,k,currphi)  &
                  & + rhomat(i+1,j-1,k,currphi)&
                  & - rhomat(i-1,j-1,k,currphi))/delx
#if probdim==3
        rho_z = 0.25*(rhomat(i,j-1,k+1,currphi)&
                  & - rhomat(i,j-1,k-1,currphi) + &
                    & rhomat(i,j,k+1,currphi)&
                  & - rhomat(i,j,k-1,currphi))/delz
#endif

!  from Shukla 2010 paper page 7419
        tmp2 = lsrho*(nx*rho_x + ny*rho_y + nz*rho_z)

! calculate first term of rhs density correction equation 23 
!     from Shukla 2010 paper page 7419
         rhom = 0.5*(rhomat(i,j-1,k,currphi)+rhomat(i,j,k,currphi))

         if(j>1) then
          netrho(i,j-1,k,currphi) = netrho(i,j-1,k,currphi) - rhom/dely
          tmprho(i,j-1,k,currphi) = tmprho(i,j-1,k,currphi) + tmp2/dely
         endif

         if(j<NJ+1) then
          netrho(i,j,k,currphi) = netrho(i,j,k,currphi) + rhom/dely  
          tmprho(i,j,k,currphi) = tmprho(i,j,k,currphi) - tmp2/dely  
         endif

         if(j>1) then
          temp(i,j-1,k,currphi+nmat) = &
             temp(i,j-1,k,currphi+nmat) + & 
             (1.0-2.0*phi(i,j-1,k,currphi))*&
             rhonycent(i,j-1,k,currphi)*netrho(i,j-1,k,currphi) + &
             rhonycent(i,j-1,k,currphi)*tmprho(i,j-1,k,currphi)
         endif

      enddo !i=1,NI
      enddo !j=1,NJ+1
      enddo !k=1,NK



      netrho = 0.00
      tmprho = 0.00
#if probdim==2
      goto 112
#endif

! ----------------------------------------------------------------------
!     THIRD (DEPTH) DIRECTION  
! ----------------------------------------------------------------------
      do k=1,NK+1
      do j=1,NJ
      do i=1,NI

! 1st need to calculate Phi, its derivatives, and normal vector 
!     at back cell face
! phi at back cell face is calculated from Psi at back cell face
! phim is phi at back cell face, and psim is psi at back cell face
         psim = 0.5*(psi(i,j,k-1,currphi)+psi(i,j,k,currphi))
         tmp = abs(psim)**(1.0/alpha)
         phim = tmp/(tmp + abs(1-psim)**(1.0/alpha))

! calculate psi derivatives at back cell face from Shukla 2010 
!     paper page 7419
! these will be used to calculate phim derivatives at back cell face
        psiz = (psi(i,j,k,currphi) - psi(i,j,k-1,currphi))/delz

        psiy = 0.25*(psi(i,j+1,k,currphi)&
                 & - psi(i,j-1,k,currphi) + &
                   & psi(i,j+1,k-1,currphi)&
                 & - psi(i,j-1,k-1,currphi))/dely

        psix = 0.25*(psi(i+1,j,k,currphi)&
                 & - psi(i-1,j,k,currphi) + &
                   & psi(i+1,j,k-1,currphi)&
                 & - psi(i-1,j,k-1,currphi))/delx

        tmp = abs(phim*(1.0-phim))**(1.0-alpha)
        tmp1 = abs(phim)**alpha + abs(1.0-phim)**alpha
        tmp2 = tmp*(tmp1*tmp1)

! phix and phiy are x and y derivatives of phi 
        phix = tmp2*psix/alpha 
        phiy = tmp2*psiy/alpha 
        phiz = tmp2*psiz/alpha 


! magnitude of gradient of phi 
        tmp = sqrt(phix**2 + phiy**2 +phiz**2+ eps*eps)


! calculate normal vector 
        nx = phix/tmp
        ny = phiy/tmp
        nz = phiz/tmp

! calculate drhodx and drhody from Shukla 2010 
!     paper page 7419
        rho_z = (rhomat(i,j,k,currphi) - rhomat(i,j,k-1,currphi))/delz

        rho_y = 0.25*(rhomat(i,j+1,k,currphi)&
                  & - rhomat(i,j-1,k,currphi) + &
                    & rhomat(i,j+1,k-1,currphi)&
                  & - rhomat(i,j-1,k-1,currphi))/dely

        rho_x = 0.25*(rhomat(i+1,j,k,currphi)&
                  & - rhomat(i-1,j,k,currphi) + &
                    & rhomat(i+1,j,k-1,currphi)&
                    & - rhomat(i-1,j,k-1,currphi))/delx

! from Shukla 2010 paper page 7419
        tmp2 = lsrho*(nx*rho_x + ny*rho_y+ nz*rho_z)

! calculate first term of rhs density correction equation 23 
!     from Shukla 2010 paper page 7419
         rhom = 0.5*(rhomat(i,j,k-1,currphi)+rhomat(i,j,k,currphi))

         if(k>1) then
          netrho(i,j,k-1,currphi) = netrho(i,j,k-1,currphi) - rhom/delz
          tmprho(i,j,k-1,currphi) = tmprho(i,j,k-1,currphi) + tmp2/delz
         endif

         if(k<NK+1) then
          netrho(i,j,k,currphi) = netrho(i,j,k,currphi) + rhom/delz
          tmprho(i,j,k,currphi) = tmprho(i,j,k,currphi) - tmp2/delz
         endif

         if(k>1) then
          temp(i,j,k-1,currphi+nmat) = &
             temp(i,j,k-1,currphi+nmat) + & 
             (1.0-2.0*phi(i,j,k-1,currphi))*&
             rhonzcent(i,j,k-1,currphi)*netrho(i,j,k-1,currphi) + &
             rhonzcent(i,j,k-1,currphi)*tmprho(i,j,k-1,currphi)
         endif

      enddo !i=1,NI
      enddo !j=1,NJ
      enddo !k=1,NK+1


112   continue 


      end subroutine calcrhorhs5
!***********************************************************************
