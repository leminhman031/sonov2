!***********************************************************************
      subroutine tempcorrection()
!***********************************************************************
! This subroutine computes both the field temperature
!   and the temperature correction within the interface region
!   which is done using shukla 2010 reinitialization


! pull required variables from respective modules
      use globalvar
      use gridvariables
      use shukla2010
      use mieeos5eqn_ntemp
      use RungKutt3   !, only: temp


      temp = 0.00
      psi = 0.00
      res = 1.0e5
      tol =tau**2
      call BC(2)
! ----------------------------------------------------------------------
!          Calculate cell center Psi with knowing Phi for (nmat) Phi
! ----------------------------------------------------------------------
! This is for (nmat) phi
      do lp = 1, nmat
      do k=(-NG+1),NK+NG
      do j=(-NG+1),NJ+NG
      do i=(-NG+1),NI+NG
          tmp = abs(phi(i,j,k,lp))**alpha
          psi(i,j,k,lp) = tmp/(tmp + abs(1-phi(i,j,k,lp))**alpha)
      enddo !i=(-NG+1),NI+NG
      enddo !j=(-NG+1),NJ+NG
      enddo !k=(-NG+1),NK+NG
      enddo ! lp=1,nmat


! ----------------------------------------------------------------------
!          Calculate normal vector components NX,NY,NZ
! ----------------------------------------------------------------------
! This is done for (nmat) phi 
      do lp = 1, nmat
      do k=1,NK
      do j=1,NJ
      do i=1,NI
! calculate psi derivatives at cell center
! from Shukla 2010 paper page 7419
! these will be used to calculate normal vector at cell center
          psix = 0.5*(psi(i+1,j,k,lp) - psi(i-1,j,k,lp))/delx
          psiy = 0.5*(psi(i,j+1,k,lp) - psi(i,j-1,k,lp))/dely
#if probdim==3
          psiz = 0.5*(psi(i,j,k+1,lp) - psi(i,j,k-1,lp))/delz
#else
          psiz = 0.00
#endif
          tmp = sqrt( psix**2 + psiy**2 + psiz**2 + eps*eps)

          rhonxcent(i,j,k,lp) = psix/tmp
          rhonycent(i,j,k,lp) = psiy/tmp
          rhonzcent(i,j,k,lp) = psiz/tmp
      enddo !i=1,NI
      enddo !j=1,NJ
      enddo !k=1,NK
      enddo !lp=1,nmat



!
! Section of code for computing temperature 
! note 
! 5eqn model is assumed implicitly on
      T=1.0
      do lp=1,nmat
        if (eos(lp).eq.2) then  ! MG EOS
         do k=1,NK
         do j=1,NJ
         do i=1,NI
            if (phi(i,j,k,lp)>=0.999) then
               p_MG = P(i,j,k)*pscale
               r1=rhomat(i,j,k,lp)*rscale/phi(i,j,k,lp)
               v_MG = 1.0/r1
               call MG_EOS(lp,p_MG,v_MG,tmp222)
               T(i,j,k,lp) = tmp222/Tempscale
               T(i,j,k,0) = tmp222/Tempscale
            endif
         enddo !i=1,NI
         enddo !j=1,NJ
         enddo !k=1,NK
        elseif (eos(lp).eq.1) then ! stiffened EOS
         do k=1,NK
         do j=1,NJ
         do i=1,NI
            if (phi(i,j,k,lp)>=0.999) then
            ts=1.0
            es=matprop(5,lp)
            ev = (P(i,j,k) + matprop(2,lp)*matprop(3,lp))&
                 /rho(i,j,k)/(matprop(2,lp)-1.0)
            T(i,j,k,lp) = ev/matprop(1,lp)
            T(i,j,k,0) = ev/matprop(1,lp)
            endif
         enddo !i=1,NI
         enddo !j=1,NJ
         enddo !k=1,NK
        endif
      enddo !lp=1,nmat
      !where(T<1) T=1.0
      call BC(5)
! End Section of code for computing temperature 
!


      iskip=1
      if (iskip.eq.1) goto 999
! ----------------------------------------------------------------------
!         Begin RK2 iteration for temp correction
! ----------------------------------------------------------------------
      niterT = 0
      resT = 1.e5
      tol_temp=0.001
      phitol2=0.0008    ! this controls inside phi=0.5
      phitol3=0.00001  ! this controls outside
      do while ( (niterT < iterT) .AND. (resT > tol_temp) )

      do k=1,NK
      do j=1,NJ 
      do i=1,NI
         temp(i,j,k,1) = T(i,j,k,0)
      enddo !i=1,NI
      enddo !j=1,NJ
      enddo !k=1,NK

! calculate RHS of temp correction equation
      call calctemprhs4(nmed) 

!     DO 1ST STAGE OF RK2
      do k=1,NK
      do j=1,NJ 
      do i=1,NI
         if( (phi(i,j,k,nmed)<(1.0-phitol2)) .AND. &
              (phi(i,j,k,nmed)>phitol3) ) then
             T(i,j,k,0) = temp(i,j,k,1) + tau*temp(i,j,k,2)
         endif
      enddo !i=1,NI
      enddo !j=1,NJ
      enddo !k=1,NK

      call BC(5) 
! calculate RHS of temp correction equation
      call calctemprhs4(nmed) 

!     DO 2nd STAGE OF RK2
      do k=1,NK
      do j=1,NJ 
      do i=1,NI
         if( (phi(i,j,k,nmed)<(1.0-phitol2)) .AND. &
              (phi(i,j,k,nmed)>phitol3) ) then
             T(i,j,k,0) = 0.5*temp(i,j,k,1) +&
                      & 0.5*T(i,j,k,0) +&
                      & 0.5*tau*temp(i,j,k,2)
         endif
      enddo !i=1,NI
      enddo !j=1,NJ
      enddo !k=1,NK

      call BC(5) 


! calculate the maximum residue occurring
      res = 0.0
      do k=1,NK
      do j=1,NJ
      do i=1,NI
         if (res < abs(temp(i,j,k,1)-T(i,j,k,0))) then
           res = abs(temp(i,j,k,1)-T(i,j,k,0))
         endif
      enddo !i=1,NI
      enddo !j=1,NJ
      enddo !k=1,NK

      resT = res
      niterT = niterT + 1

      enddo 


! ----------------------------------------------------------------------
!     END RK2 iteration for temperature correction
! ----------------------------------------------------------------------

 999  continue

      end subroutine tempcorrection
!***********************************************************************
