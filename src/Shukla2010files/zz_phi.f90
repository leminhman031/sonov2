!***********************************************************************
      subroutine zz_phi(dl_vol,phatvol)
!***********************************************************************
! This subroutine applies Shukla's interface correction scheme from 2010 
! this scheme has correction factor for volume only

! pull required variables from respective modules
      use globalvar
      use gridvariables
      use Shukla2010
      use mieeos5eqn_ntemp

      real, intent(inout),dimension(num_particles) :: dl_vol, phatvol


! ----------------------------------------------------------------------
!     Begin Iteration to correct phi while conserving mass/vol
! ----------------------------------------------------------------------
      call BC(2)
! SET ORIGINAL PHI FIELD TO TEMPORARY ARRAY

      tempphi = phi



! following is case/switch code based on rk_reinit
! if rk_reinit=2 then do RK2 scheme
! if rk_reinit=3 then do RK3 scheme

      select case (rk_reinit)
!   2 STAGE R-K SCHEME
      case (2)

!!!!! DO 1ST STAGE OF RK2
! calculate RHS of phi correction equation
      do lp = 1, nmat
        if(lp.ne.nmed)call calcphirhs(lp)
      enddo


      do lp = 1,nmat
       if(lp.ne.nmed)then
        do k=1,NK
        do j=1,NJ 
        do i=1,NI
          phi(i,j,k,lp) = tempphi(i,j,k,lp) + tau*tempphi2(i,j,k,lp)
          if( lp.gt.nmed .and. &
              phi(i,j,k,lp)<(1.0-phitol) .AND. &
              phi(i,j,k,lp)>phitol ) then
            phi(i,j,k,lp) = phi(i,j,k,lp) + tau*dl_vol(lp-nmed)
          endif
        enddo !i=1,NI
        enddo !j=1,NJ
        enddo 
       endif
      enddo 

      where (phi<0.00) phi=0.0000
      where (phi>1.00) phi=1.0000
! apply phi boundary condition using 1st stage phi
      call BC(2)


!!!!! DO 2nd STAGE OF RK2
! calculate RHS of phi correction equation
      do lp = 1, nmat
        if(lp.ne.nmed)call calcphirhs(lp)
      enddo

      do lp = 1,nmat
       if(lp.ne.nmed)then
        do k=1,NK
        do j=1,NJ 
        do i=1,NI
            phi(i,j,k,lp) = 0.5*tempphi(i,j,k,lp) +&
                        & 0.5*phi(i,j,k,lp) +&
                        & 0.5*tau*tempphi2(i,j,k,lp)
          if( lp.gt.nmed .and. &
              phi(i,j,k,lp)<(1.0-phitol) .AND. &
              phi(i,j,k,lp)>phitol ) then
            phi(i,j,k,lp) = phi(i,j,k,lp) + 0.5*tau*dl_vol(lp-nmed)
          endif
        enddo !i=1,NI
        enddo !j=1,NJ
        enddo 
       endif
      enddo 

      where (phi<0.00) phi=0.0000
      where (phi>1.00) phi=1.0000
! apply phi boundary condition using 2nd stage phi
      call BC(2)



!_______________________________________________________
!     3 STAGE R-K SCHEME
      case (3)

!!!!! DO 1ST STAGE OF RK3

! calculate RHS of phi correction equation
      do lp = 1, nmat
        if(lp.ne.nmed)call calcphirhs(lp)
      enddo
      do lp = 1,nmat
       if(lp.ne.nmed)then
        do k=1,NK
        do j=1,NJ 
        do i=1,NI
            phi(i,j,k,lp) = tempphi(i,j,k,lp) + tau*tempphi2(i,j,k,lp)
          if( lp.gt.nmed .and. &
              phi(i,j,k,lp)<(1.0-phitol) .AND. &
              phi(i,j,k,lp)>phitol ) then
            phi(i,j,k,lp) = phi(i,j,k,lp) + tau*dl_vol(lp-nmed)
          endif
        enddo !i=1,NI
        enddo !j=1,NJ
        enddo 
       endif
      enddo 

      where (phi<0.00) phi=0.0000
      where (phi>1.00) phi=1.0000
! apply phi boundary condition using 1st stage phi
      call BC(2)



!!!!! DO 2nd STAGE OF RK3
! calculate RHS of phi correction equation
      do lp = 1, nmat
        if(lp.ne.nmed)call calcphirhs(lp)
      enddo

      do lp = 1,nmat
       if(lp.ne.nmed)then
        do k=1,NK
        do j=1,NJ 
        do i=1,NI
          phi(i,j,k,lp) = 0.75*tempphi(i,j,k,lp) +&
                        & 0.25*phi(i,j,k,lp) +&
                        & 0.25*tau*tempphi2(i,j,k,lp)
          if( lp.gt.nmed .and. &
              phi(i,j,k,lp)<(1.0-phitol) .AND. &
              phi(i,j,k,lp)>phitol ) then
            phi(i,j,k,lp) = phi(i,j,k,lp) + 0.25*tau*dl_vol(lp-nmed)
          endif
        enddo !i=1,NI
        enddo !j=1,NJ
        enddo 
       endif
      enddo 

      where (phi<0.00) phi=0.0000
      where (phi>1.00) phi=1.0000
! apply phi boundary condition 
      call BC(2)



!!!!! DO 3rd STAGE OF RK3
! calculate RHS of phi correction equation
      do lp = 1, nmat
        if(lp.ne.nmed)call calcphirhs(lp)
      enddo

      do lp = 1,nmat
       if(lp.ne.nmed)then
        do k=1,NK
        do j=1,NJ 
        do i=1,NI
          phi(i,j,k,lp) = (1.0/3.0)*tempphi(i,j,k,lp) +&
                        & (2.0/3.0)*phi(i,j,k,lp) +&
                        & (2.0/3.0)*tau*tempphi2(i,j,k,lp)
          if( lp.gt.nmed .and. &
              phi(i,j,k,lp)<(1.0-phitol) .AND. &
              phi(i,j,k,lp)>phitol ) then
           phi(i,j,k,lp) = phi(i,j,k,lp) + (2.0/3.0)*tau*dl_vol(lp-nmed)
          endif
        enddo !i=1,NI
        enddo !j=1,NJ
        enddo 
       endif
      enddo 

      where (phi<0.00) phi=0.0000
      where (phi>1.00) phi=1.0000
! apply phi boundary condition 
      call BC(2)

      end select


! set used arrays to 0 before leaving function
      tempphi = 0.0000
      tempphi2 = 0.0000

!  CALCULATE PARTICLE VOL
      phatvol = 0.0000
      do k=1,NK
      do j=1,NJ
      do i=1,NI
        do lp=1,num_particles
         phatvol(lp) = phatvol(lp) + phi(i,j,k,nmed+lp)*vol(i,j,k)
        enddo
      enddo
      enddo
      enddo



      end subroutine zz_phi
!***********************************************************************
