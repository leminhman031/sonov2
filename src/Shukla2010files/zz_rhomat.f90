!***********************************************************************
      subroutine zz_rhomat(dl_mass,phatmass)
!***********************************************************************
! This subroutine applies Shukla's interface correction scheme from 2010 
! this scheme has correction factor for mass

! pull required variables from respective modules
      use globalvar
      use gridvariables
      use Shukla2010
      use mieeos5eqn_ntemp
      use RungKutt3, only: temp

      real, intent(in),dimension(num_particles) :: dl_mass
      real, intent(out),dimension(num_particles) :: phatmass

      temp = 0.00


! ----------------------------------------------------------------------
!     Begin Iteration to correct phi while conserving mass/vol
! ----------------------------------------------------------------------
! SET ORIGINAL RHOMAT FIELD TO TEMPORARY ARRAY
! Assign 0-stage RK values of RHOMAT to temp array
      temprhomat = rhomat
! SET ORIGINAL RHO FIELD TO TEMPORARY ARRAY
! Assign 0-stage RK values of RHO to temp array
      do k=1,NK
      do j=1,NJ 
      do i=1,NI
         temprho(i,j,k,1) = rho(i,j,k)
      enddo !i=1,NI
      enddo !j=1,NJ
      enddo !k

! following is case/switch code based on rk_reinit
! if rk_reinit=2 then do RK2 scheme
! if rk_reinit=3 then do RK3 scheme

      select case (rk_reinit)

!   2 STAGE R-K SCHEME
      case (2)
!!!!! DO 1ST STAGE OF RK2
! calculate RHS of rho correction equation
! calculate RHS of rho correction equation
      call calcrhorhs4(nmed) 
      do lp = 1, nmat
! calculate RHS of rho correction equation
        if(lp.ne.nmed)call calcrhorhs5(lp)
      enddo
! DO 1ST STAGE OF RK2 FOR DENSITY
      do k=1,NK
      do j=1,NJ 
      do i=1,NI
         if( (phi(i,j,k,nmed)<(1.0-phitol)) .AND. &
              (phi(i,j,k,nmed)>phitol) ) then
            rho(i,j,k) = temprho(i,j,k,1) + tau*temp(i,j,k,2)
            do lp = nmed+1, nmat
               rhomat(i,j,k,lp) = temprhomat(i,j,k,lp)&
                & + tau*temp(i,j,k,lp+nmat) &
                & + tau*dl_mass(lp-nmed)
            enddo
            do lp = 1, nmed
               if(lp.ne.nmed)then
               rhomat(i,j,k,lp) = temprhomat(i,j,k,lp)&
                  & + tau*temp(i,j,k,lp+nmat)
               endif
            enddo
         endif
      enddo !i=1,NI
      enddo !j=1,NJ
      enddo !k

! apply rho boundary condition using 1st stage rho
      call BC(4)

!!!!! DO 2nd STAGE OF RK2
! calculate RHS of rho correction equation
! calculate RHS of rho correction equation
      call calcrhorhs4(nmed) 
      do lp = 1, nmat
! calculate RHS of rho correction equation
        if(lp.ne.nmed)call calcrhorhs5(lp)
      enddo
! DO 2nd STAGE OF RK2 for density
      do k=1,NK
      do j=1,NJ 
      do i=1,NI
         if( (phi(i,j,k,nmed)<(1.0-phitol)) .AND. &
              (phi(i,j,k,nmed)>phitol) ) then
            rho(i,j,k) = 0.5*temprho(i,j,k,1) +&
                     & 0.5*rho(i,j,k) +&
                     & 0.5*tau*temp(i,j,k,2)
            do lp = nmed+1, nmat
               rhomat(i,j,k,lp) = 0.5*temprhomat(i,j,k,lp) +&
                              & 0.5*rhomat(i,j,k,lp) +&
                              & 0.5*tau*temp(i,j,k,lp+nmat) &
                              & + 0.5*tau*dl_mass(lp-nmed)
            enddo
            do lp = 1, nmed
              if(lp.ne.nmed)then
               rhomat(i,j,k,lp) = 0.5*temprhomat(i,j,k,lp) +&
                              & 0.5*rhomat(i,j,k,lp) +&
                              & 0.5*tau*temp(i,j,k,lp+nmat)
              endif
            enddo
         endif
      enddo !i=1,NI
      enddo !j=1,NJ
      enddo !k
! apply rho boundary condition 
      call BC(3)
      call BC(4)


!_______________________________________________________
!     3 STAGE R-K SCHEME
      case (3)
!!!!! DO 1ST STAGE OF RK3

! calculate RHS of rho correction equation
      call calcrhorhs4(nmed) 
      do lp = 1, nmat
! calculate RHS of rho correction equation
        if(lp.ne.nmed)call calcrhorhs5(lp)
      enddo

! DO 1ST STAGE OF RK3 FOR DENSITY
      do k=1,NK
      do j=1,NJ 
      do i=1,NI
         if( (phi(i,j,k,nmed)<(1.0-phitol)) .AND. &
              (phi(i,j,k,nmed)>phitol) ) then
            rho(i,j,k) = temprho(i,j,k,1) + tau*temp(i,j,k,2)
            do lp = nmed+1, nmat
               rhomat(i,j,k,lp) = temprhomat(i,j,k,lp)&
                    & + tau*temp(i,j,k,lp+nmat) &
                    & + tau*dl_mass(lp-nmed)
            enddo
            do lp = 1, nmed
              if(lp.ne.nmed)then
               rhomat(i,j,k,lp) = temprhomat(i,j,k,lp)&
                          & + tau*temp(i,j,k,lp+nmat)
              endif
            enddo
         endif
      enddo !i=1,NI
      enddo !j=1,NJ
      enddo !k
! apply rho boundary condition using 1st stage rho
      call BC(3) 
      call BC(4)



!!!!! DO 2nd STAGE OF RK3
! calculate RHS of rho correction equation
      call calcrhorhs4(nmed) 
      do lp = 1, nmat
! calculate RHS of rho correction equation
        if(lp.ne.nmed)call calcrhorhs5(lp)
      enddo
! DO 2nd STAGE OF RK3 for density
      do k=1,NK
      do j=1,NJ 
      do i=1,NI
         if( (phi(i,j,k,nmed)<(1.0-phitol)) .AND. &
              (phi(i,j,k,nmed)>phitol) ) then
            rho(i,j,k) = 0.75*temprho(i,j,k,1) +&
                     & 0.25*rho(i,j,k) +&
                     & 0.25*tau*temp(i,j,k,2)
            do lp = nmed+1, nmat
               rhomat(i,j,k,lp) = 0.75*temprhomat(i,j,k,lp) +&
                              & 0.25*rhomat(i,j,k,lp) +&
                              & 0.25*tau*temp(i,j,k,lp+nmat) &
                              & + 0.25*tau*dl_mass(lp-nmed)
            enddo
            do lp = 1, nmed
              if(lp.ne.nmed)then
               rhomat(i,j,k,lp) = 0.75*temprhomat(i,j,k,lp) +&
                              & 0.25*rhomat(i,j,k,lp) +&
                              & 0.25*tau*temp(i,j,k,lp+nmat)
              endif
            enddo
         endif
      enddo !i=1,NI
      enddo !j=1,NJ
      enddo !k
! apply rho boundary condition 
      call BC(3)
      call BC(4)

!!!!! DO 3rd STAGE OF RK3
! calculate RHS of rho correction equation
! calculate RHS of rho correction equation
      call calcrhorhs4(nmed) 
      do lp = 1, nmat
! calculate RHS of rho correction equation
        if(lp.ne.nmed)call calcrhorhs5(lp)
      enddo
      do k=1,NK
      do j=1,NJ 
      do i=1,NI
         if( (phi(i,j,k,nmed)<(1.0-phitol)) .AND. &
              (phi(i,j,k,nmed)>phitol) ) then
            rho(i,j,k) = (1.0/3.0)*temprho(i,j,k,1) +&
                     & (2.0/3.0)*rho(i,j,k) +&
                     & (2.0/3.0)*tau*temp(i,j,k,2)
            do lp = nmed+1, nmat
               rhomat(i,j,k,lp) = (1.0/3.0)*temprhomat(i,j,k,lp) +&
                           & (2.0/3.0)*rhomat(i,j,k,lp) +&
                           & (2.0/3.0)*tau*temp(i,j,k,lp+nmat) &
                           & + (2.0/3.0)*tau*dl_mass(lp-nmed) 
            enddo
            do lp = 1, nmed
              if(lp.ne.nmed)then
               rhomat(i,j,k,lp) = (1.0/3.0)*temprhomat(i,j,k,lp) +&
                           & (2.0/3.0)*rhomat(i,j,k,lp) +&
                           & (2.0/3.0)*tau*temp(i,j,k,lp+nmat)
              endif
            enddo
         endif
      enddo !i=1,NI
      enddo !j=1,NJ
      enddo !k
! apply rho boundary condition 
      call BC(3)
      call BC(4)

      end select


! update the last material rhomat array (nmed)
      do k=1,NK
      do j=1,NJ
      do i=1,(NI)
        tmp=0.00
        do lp=1,nmat
         if(lp.ne.nmed)tmp=tmp+rhomat(i,j,k,lp)
        enddo
        rhomat(i,j,k,nmed) = rho(i,j,k)-tmp
      enddo
      enddo
      enddo
      call BC(4) ! apply rho boundary condition


!  CALCULATE PARTICLE MASS
      phatmass = 0.0000
      do k=1,NK
      do j=1,NJ
      do i=1,NI
        do lp=1,num_particles
         phatmass(lp) = phatmass(lp) + rhomat(i,j,k,nmed+lp)*vol(i,j,k)
        enddo
      enddo
      enddo
      enddo


! set used arrays to 0 before leaving function
      temprho = 0.0000
      temp = 0.0000




      end subroutine zz_rhomat
!***********************************************************************
