!***********************************************************************
      subroutine rk3()
!***********************************************************************
! Note 5-equation model is implicitly assumed on
! Note rhomat = rho*phi is implicitly assumed on
! pull required variables from respective modules

      use globalvar
      use gridvariables
      use Shukla2010
      use TVDfunc, only: netFlux,lphi1,lphi2,rphi1,rphi2,locx,locy
      use RungKutt3
      use mieeos5eqn_ntemp


      temp = 0.00
      state = 0.00
      netFlux = 0.00
      fluidprop = 0.00


! ----------------------------------------------------------------------
!     PRELIMINARY TO STEP 1: APPLY BOUNDARY CONDITION AND 
!        COPY INITIAL DATA
! ----------------------------------------------------------------------

! apply boundary condition
      call updatelastphi
      call updatemixrho
      call BC(1) 

! calculate the conservative variables at each cell
      do k=(-NG+1),NK+NG
      do j=(-NG+1),NJ+NG
      do i=(-NG+1),NI+NG
! assign primitive variables to state array, which will be input for 
! the function calcfluidprop

        do imat=1,nmat
         state(6+nmat+imat)= rhomat(i,j,k,imat)
        enddo

        state(1) = rho(i,j,k)
        state(2) = u(i,j,k)
        state(3) = 0.0
        state(4) = 0.0

#if probdim==2
        state(3) = v(i,j,k)
#endif

#if probdim==3
        state(3) = v(i,j,k)
        state(4) = w(i,j,k)
#endif

        state(5) = p(i,j,k)
      ! state(6) is for temperature, isn't used 

        do loop = 1,nmat
         state(6+loop) = phi(i,j,k,loop)
        enddo 

! calculate the fluid properties 
! note fluidprop(4) is conservative E
        call calcfluidprop(state,fluidprop,i,j,k,1)

! calculate rest of conservative variables
! note conservative E is already calculated in calcfluidprop
        rhou(i,j,k) = rho(i,j,k)*u(i,j,k)
        rhoE(i,j,k) = fluidprop(4)
#if probdim==2
        rhov(i,j,k) = rho(i,j,k)*v(i,j,k)
#endif

#if probdim==3
        rhov(i,j,k) = rho(i,j,k)*v(i,j,k)
        rhow(i,j,k) = rho(i,j,k)*w(i,j,k)
#endif
      enddo !i=(-NG+1),NI+NG
      enddo !j=(-NG+1),NJ+NG
      enddo !k=(-NG+1),NK+NG


!_______________________________________________________________________

! allocate the stage0 conservative variables to temp array
      temp(:,:,:,1) = rho(:,:,:)
      temp(:,:,:,2) = rhou(:,:,:)
#if probdim==2
      temp(:,:,:,3) = rhov(:,:,:)

#elif probdim==3
      temp(:,:,:,3) = rhov(:,:,:)
      temp(:,:,:,4) = rhow(:,:,:)
#endif

      temp(:,:,:,5) = rhoE(:,:,:)
      temp(:,:,:,6+1:6+nmat) = phi(:,:,:,1:nmat)

      lp = 6+nmat
      temp(:,:,:,lp+1:lp+nmat) = rhomat(:,:,:,1:nmat)

      lp = 6+nmat+nmat
      temp(:,:,:,lp+1:lp+nspec) = rhoY(:,:,:,1:nspec)

!_______________________________________________________________________


! ----------------------------------------------------------------------
!     STEP 1: 1st Stage of Rung-Kutta 3
! ----------------------------------------------------------------------
! call tvd function to update the net flux array to be used for RK3   
      call tvd


      if (CFL > 0.00) then
         deltat = CFL*amin1(delx,dely,delz)/maxspeed
      endif


#if  REACT==1 
      call updatereaction
#endif


!_______________________________________________________________________
!*********************** FIRST STAGE OF RK3 ****************************
 
      rho(1:NI,1:NJ,1:NK) = temp(1:NI,1:NJ,1:NK,1) + &
                          & deltat*netFlux(1:NI,1:NJ,1:NK,1)

      rhou(1:NI,1:NJ,1:NK) = temp(1:NI,1:NJ,1:NK,2) + & 
                           & deltat*netFlux(1:NI,1:NJ,1:NK,2)


#if probdim==1
      go to 222
#endif

 
      rhov(1:NI,1:NJ,1:NK) = temp(1:NI,1:NJ,1:NK,3) + & 
                           & deltat*netFlux(1:NI,1:NJ,1:NK,3)


#if probdim==2
      go to 222
#endif

 
      rhow(1:NI,1:NJ,1:NK) = temp(1:NI,1:NJ,1:NK,4) + &
                           & deltat*netFlux(1:NI,1:NJ,1:NK,4)


222   continue

 
      rhoE(1:NI,1:NJ,1:NK) = temp(1:NI,1:NJ,1:NK,5) + & 
                           & deltat*netFlux(1:NI,1:NJ,1:NK,5)


      lp = nmat
      phi(1:NI,1:NJ,1:NK,1:lp) = temp(1:NI,1:NJ,1:NK,6+1:6+lp) + &
                     &  deltat*netFlux(1:NI,1:NJ,1:NK,6+1:6+lp)


      lp = 6+nmat
      rhomat(1:NI,1:NJ,1:NK,1:nmat) = & 
                        & temp(1:NI,1:NJ,1:NK,lp+1:lp+nmat) + &
                        & deltat*netFlux(1:NI,1:NJ,1:NK,lp+1:lp+nmat)



#if  REACT==1 
      lp = 6+nmat+nmat
      rhoY(1:NI,1:NJ,1:NK,1:nspec) = & 
                        & temp(1:NI,1:NJ,1:NK,lp+1:lp+nspec) + &
                        & deltat*netFlux(1:NI,1:NJ,1:NK,lp+1:lp+nspec)
      where (rhoY<0) rhoY=0.0
#endif
!_______________________________________________________________________



! update last material phi 
! update fluid rho (because 5eqn assumed on)
      call updatelastphi
      call updatemixrho

! ----------------------------------------------------------------------
!     PRELIMINARY TO STEP 2: USE CONSERVATIVE VAR TO CALC PRIMITIVE VAR
! ----------------------------------------------------------------------
      do k=1,NK
      do j=1,NJ
      do i=1,NI
! assign primitive variables to state array, which will be input for 
! the function calcfluidprop
! Note 5-equation model is implicitly assumed
! Note rhomat = rho*phi is implicitly assumed
        do imat=1,nmat
         state(6+nmat+imat)= rhomat(i,j,k,imat)
        enddo

         do loop = 1,nmat
            state(6+loop) = phi(i,j,k,loop)
         enddo 

! calculate the fluid gamma and fluid p_infinity for cell (i,j,k)
! note fluidprop(1) is fluid gamma and fluidprop(2) is fluid p_infinity
         call calcfluidprop(state,fluidprop,i,j,k,0)

! calculate prim variable (u,v,P) from conserv var (rhou, rhov,E) 
! note conservative E is already calculated in calcfluidprop
         u(i,j,k) = rhou(i,j,k)/rho(i,j,k)
         tmp = rhou(i,j,k)**2

#if probdim==2
         tmp = tmp + rhov(i,j,k)**2
         v(i,j,k) = rhov(i,j,k)/rho(i,j,k)

#elif probdim==3
         tmp = tmp + rhov(i,j,k)**2 + rhow(i,j,k)**2
         v(i,j,k) = rhov(i,j,k)/rho(i,j,k)
         w(i,j,k) = rhow(i,j,k)/rho(i,j,k)
#endif

         p(i,j,k) = (fluidprop(1)-1)*(rhoE(i,j,k)-0.5*tmp/rho(i,j,k)) & 
                  & - fluidprop(1)*fluidprop(2)

      enddo !i=1,NI
      enddo !j=1,NJ
      enddo 
!_______________________________________________________________________




! ----------------------------------------------------------------------
!                   STEP 2: 2nd Stage of Rung-Kutta 3
! ----------------------------------------------------------------------
! apply boundary condition 
        call BC(1) 

! call tvd function to update the net flux array to be used for RK3
      call tvd

#if  REACT==1 
      call updatereaction
#endif


!_______________________________________________________________________
!*********************** SECOND STAGE OF RK3 ***************************

      tmp = 0.75
      tmp2 = 0.25

      rho(1:NI,1:NJ,1:NK) = temp(1:NI,1:NJ,1:NK,1)*tmp + &
                           & rho(1:NI,1:NJ,1:NK)*tmp2  + &
                           & netFlux(1:NI,1:NJ,1:NK,1)*deltat*tmp2



      rhou(1:NI,1:NJ,1:NK) = temp(1:NI,1:NJ,1:NK,2)*tmp + & 
                           & rhou(1:NI,1:NJ,1:NK)*tmp2  + &
                           & netFlux(1:NI,1:NJ,1:NK,2)*deltat*tmp2


#if probdim==1
      go to 223
#endif


      rhov(1:NI,1:NJ,1:NK) = temp(1:NI,1:NJ,1:NK,3)*tmp + & 
                           & rhov(1:NI,1:NJ,1:NK)*tmp2  + &
                           & netFlux(1:NI,1:NJ,1:NK,3)*deltat*tmp2


#if probdim==2
      goto 223
#endif


      rhow(1:NI,1:NJ,1:NK) = temp(1:NI,1:NJ,1:NK,4)*tmp + &
                           & rhow(1:NI,1:NJ,1:NK)*tmp2  + &
                           & netFlux(1:NI,1:NJ,1:NK,4)*deltat*tmp2


223   continue


      rhoE(1:NI,1:NJ,1:NK) = temp(1:NI,1:NJ,1:NK,5)*tmp + & 
                           & rhoE(1:NI,1:NJ,1:NK)*tmp2  + &
                           & netFlux(1:NI,1:NJ,1:NK,5)*deltat*tmp2


      lp = nmat
      phi(1:NI,1:NJ,1:NK,1:lp) = & 
                          & temp(1:NI,1:NJ,1:NK,6+1:6+lp)*tmp      + &
                          & phi(1:NI,1:NJ,1:NK,1:lp)*tmp2      + &
                          & netFlux(1:NI,1:NJ,1:NK,6+1:6+lp)*deltat*tmp2

      lp = 6+nmat
      rhomat(1:NI,1:NJ,1:NK,1:nmat) = & 
                    & temp(1:NI,1:NJ,1:NK,lp+1:lp+nmat)*tmp   + &
                    & rhomat(1:NI,1:NJ,1:NK,1:nmat)*tmp2      + &
                    & netFlux(1:NI,1:NJ,1:NK,lp+1:lp+nmat)*deltat*tmp2


#if  REACT==1 
      lp = 6+nmat+nmat

      rhoY(1:NI,1:NJ,1:NK,1:nspec) = & 
                    & temp(1:NI,1:NJ,1:NK,lp+1:lp+nspec)*tmp + &
                    & rhoY(1:NI,1:NJ,1:NK,1:nspec)*tmp2      + &
                    & netFlux(1:NI,1:NJ,1:NK,lp+1:lp+nspec)*deltat*tmp2 
      where (rhoY<0) rhoY=0.0

#endif
!_______________________________________________________________________


! update last material phi 
! update fluid rho (because 5eqn assumed on)
      call updatelastphi
      call updatemixrho


! ----------------------------------------------------------------------
!    PRELIMINARY TO STEP 3: USE CONSERVATIVE VAR TO CALC PRIMITIVE VAR
! ----------------------------------------------------------------------
      do k=1,NK
      do j=1,NJ
      do i=1,NI
! assign primitive variables to state array, which will be input for 
! the function calcfluidprop
! Note 5-equation model is implicitly assumed
! Note rhomat = rho*phi is implicitly assumed
        do imat=1,nmat
         state(6+nmat+imat)= rhomat(i,j,k,imat)
        enddo

         do loop = 1,nmat
            state(6+loop) = phi(i,j,k,loop)
         enddo 
 
! calculate the fluid gamma and fluid p_infinity for cell (i,j,k)
! note fluidprop(1) is fluid gamma and fluidprop(2) is fluid p_infinity
         call calcfluidprop(state,fluidprop,i,j,k,0)

! calculate prim variable (u,v,P) from conserv var (rhou, rhov,E) 
! note conservative E is already calculated in calcfluidprop
         u(i,j,k) = rhou(i,j,k)/rho(i,j,k)
         tmp = rhou(i,j,k)**2

#if probdim==2
         tmp = tmp + rhov(i,j,k)**2
         v(i,j,k) = rhov(i,j,k)/rho(i,j,k)

#elif probdim==3
         tmp = tmp + rhov(i,j,k)**2 + rhow(i,j,k)**2
         v(i,j,k) = rhov(i,j,k)/rho(i,j,k)
         w(i,j,k) = rhow(i,j,k)/rho(i,j,k)
#endif

         p(i,j,k) = (fluidprop(1)-1)*(rhoE(i,j,k)-0.5*tmp/rho(i,j,k)) & 
                  & - fluidprop(1)*fluidprop(2)

      enddo !i=1,NI
      enddo !j=1,NJ
      enddo 
!_______________________________________________________________________


! ----------------------------------------------------------------------
!                   STEP 3: 3rd Stage of Rung-Kutta 3
! ----------------------------------------------------------------------
! apply boundary condition
      call BC(1)

! call tvd function to update the net flux array to be used for RK3   
       call tvd


#if  REACT==1 
      call updatereaction
#endif


!_______________________________________________________________________
!************************ THIRD STAGE OF RK3 ***************************
      tmp = (1.0/3.0)
      tmp2 = (2.0/3.0)

      rho(1:NI,1:NJ,1:NK) = temp(1:NI,1:NJ,1:NK,1)*tmp + &
                           & rho(1:NI,1:NJ,1:NK)*tmp2  + &
                           & netFlux(1:NI,1:NJ,1:NK,1)*deltat*tmp2



      rhou(1:NI,1:NJ,1:NK) = temp(1:NI,1:NJ,1:NK,2)*tmp + & 
                           & rhou(1:NI,1:NJ,1:NK)*tmp2  + &
                           & netFlux(1:NI,1:NJ,1:NK,2)*deltat*tmp2


#if probdim==1
      go to 224 
#endif


      rhov(1:NI,1:NJ,1:NK) = temp(1:NI,1:NJ,1:NK,3)*tmp + & 
                           & rhov(1:NI,1:NJ,1:NK)*tmp2  + &
                           & netFlux(1:NI,1:NJ,1:NK,3)*deltat*tmp2


#if probdim==2
      go to 224
#endif


      rhow(1:NI,1:NJ,1:NK) = temp(1:NI,1:NJ,1:NK,4)*tmp + &
                           & rhow(1:NI,1:NJ,1:NK)*tmp2  + &
                           & netFlux(1:NI,1:NJ,1:NK,4)*deltat*tmp2


224   continue


      rhoE(1:NI,1:NJ,1:NK) = temp(1:NI,1:NJ,1:NK,5)*tmp + & 
                           & rhoE(1:NI,1:NJ,1:NK)*tmp2  + &
                           & netFlux(1:NI,1:NJ,1:NK,5)*deltat*tmp2


      lp = nmat

      phi(1:NI,1:NJ,1:NK,1:lp) = & 
                          & temp(1:NI,1:NJ,1:NK,6+1:6+lp)*tmp      + &
                          & phi(1:NI,1:NJ,1:NK,1:lp)*tmp2      + &
                          & netFlux(1:NI,1:NJ,1:NK,6+1:6+lp)*deltat*tmp2


      lp = 6+nmat

      rhomat(1:NI,1:NJ,1:NK,1:nmat) = & 
                    & temp(1:NI,1:NJ,1:NK,lp+1:lp+nmat)*tmp   + &
                    & rhomat(1:NI,1:NJ,1:NK,1:nmat)*tmp2      + &
                    & netFlux(1:NI,1:NJ,1:NK,lp+1:lp+nmat)*deltat*tmp2


#if  REACT==1 
      lp = 6+nmat+nmat

      rhoY(1:NI,1:NJ,1:NK,1:nspec) = & 
                    & temp(1:NI,1:NJ,1:NK,lp+1:lp+nspec)*tmp + &
                    & rhoY(1:NI,1:NJ,1:NK,1:nspec)*tmp2      + &
                    & netFlux(1:NI,1:NJ,1:NK,lp+1:lp+nspec)*deltat*tmp2 
      where (rhoY<0) rhoY=0.0

#endif
!_______________________________________________________________________

! update last material phi 
! update fluid rho (because 5eqn assumed on)
      call updatelastphi
      call updatemixrho


! ----------------------------------------------------------------------
!             USE CONSERVATIVE VAR TO CALC PRIMITIVE VAR
! ----------------------------------------------------------------------
      do k=1,NK
      do j=1,NJ
      do i=1,NI
! assign primitive variables to state array, which will be input for 
! the function calcfluidprop
! Note 5-equation model is implicitly assumed
! Note rhomat = rho*phi is implicitly assumed
        do imat=1,nmat
         state(6+nmat+imat)= rhomat(i,j,k,imat)
        enddo

         do loop = 1,nmat
            state(6+loop) = phi(i,j,k,loop)
         enddo 
 
! calculate the fluid gamma and fluid p_infinity for cell (i,j,k)
! note fluidprop(1) is fluid gamma and fluidprop(2) is fluid p_infinity
         call calcfluidprop(state,fluidprop,i,j,k,0)

! calculate prim variable (u,v,P) from conserv var (rhou, rhov,E) 
! note conservative E is already calculated in calcfluidprop
         u(i,j,k) = rhou(i,j,k)/rho(i,j,k)
         tmp = rhou(i,j,k)**2

#if probdim==2
         tmp = tmp + rhov(i,j,k)**2
         v(i,j,k) = rhov(i,j,k)/rho(i,j,k)

#elif probdim==3
         tmp = tmp + rhov(i,j,k)**2 + rhow(i,j,k)**2
         v(i,j,k) = rhov(i,j,k)/rho(i,j,k)
         w(i,j,k) = rhow(i,j,k)/rho(i,j,k)
#endif

         p(i,j,k) = (fluidprop(1)-1)*(rhoE(i,j,k)-0.5*tmp/rho(i,j,k)) & 
                  & - fluidprop(1)*fluidprop(2)

      enddo !i=1,NI
      enddo !j=1,NJ
      enddo 

      netFlux = 0.000

! apply boundary condition
      call BC(1) 

      if (iterT>0) call tempcorrection()

! Do interface correction and apply boundary conditions
      if( (timeT > 0) .AND. (mod(giter,intiter)==0)) then

#if   INTERFACECORRECTION==0
        call shukla2010five
        call updatelastphi

#elif INTERFACECORRECTION==1
      call secant_mass_zz

#elif INTERFACECORRECTION==2
      call secant_volmass_zz

#elif INTERFACECORRECTION==3
      call shapecorrect
      call secant_volmass_zz()
#endif


      endif


      end subroutine rk3
!***********************************************************************
