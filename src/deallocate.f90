!***********************************************************************
      subroutine dealloc()
!***********************************************************************
! This subroutine DEallocates memory 

! pull required variables from respective modules
      use globalvar 
      use globalvar 
      use gridvariables
      use TVDfunc
      use RungKutt3
      use Shukla2010
      use hllc

      print*, 'BEGINNING DEALLOCATION OF ARRAYS IN ALL MODULES'

! deAllocate arrays for primitive variables from Globvar Module
      deallocate( rho,u,v,w,p,T,phi,psi,rhomat,&
                  soundspeed,phistart,phimoved,&
                  pmass,pvol,xc0,yc0,zc0,g1,g2,vlm0,vlm1,& 
                  origvol,origmass,radius,norml,lamda0,& 
                  lamda1,lamda_vol,lamda_mass,lamdanew,& 
                  partcentr,partcentr_old)
      print*, '   Finished deallocation for Globvar Module '

!  deAllocate the arrays in Gridvariables module
      deallocate(xc,yc,zc,vol,x,y,z)
      print*, '   Finished deallocation for Gridvariables Module'

! deAllocate arrays in hllc module
      deallocate( ConsvarL,ConsvarR,URstar,&
                  ULstar,FRstar,FLstar,FR,FL,D )
      print*, '   Finished deallocation for HLLC Module'

! deAllocate arrays in TVDFunc module
      deallocate( netFlux,leftstateprim,rightstateprim,flx )
      print*, '   Finished deallocation for TVDFunc Module'

! deAllocate arrays from RungKutt3 module
      deallocate( temp,rhou,rhov,rhow,rhoE,rhoY,state )
      print*, '   Finished deallocation for RungKutt3 Module'

! deAllocate arrays from Shukla2010 module
      deallocate( phinxcent,phinycent,phinzcent )
      deallocate( rhonxcent,rhonycent,rhonzcent )
      deallocate( netphi,netrho,tmprho )
      deallocate( temprho,temprho3 )
      deallocate( temprhomat,temprhomat3 )
      deallocate( tempphi,tempphi2,tempphi3 )
      print*, '   Finished deallocation for Shukla2010 Module'
      print*, 'FINISHED DEALLOCATION OF ARRAYS '
      print*, ' '
      print*, ' '

      end subroutine dealloc
!***********************************************************************
