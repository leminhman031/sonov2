!***********************************************************************
      subroutine alloc()
!***********************************************************************
! This subroutine allocates memory for arrays 
!      (some arrays allocated in readinput.f90) 



! pull required variables from respective modules
      use globalvar 
      use gridvariables
      use TVDfunc
      use RungKutt3
      use Shukla2010
      use hllc

      print*, 'BEGINNING ALLOCATION OF ARRAYS IN ALL MODULES'

! variables that make it easier to set the lengths of arrays
! arraydima: rho, u, v, w, p, T, 
!            Phi_1 to Phi_(nmat),
!            rhomat_1 to rhomat_(nmat),
!            rhoY_1 to rhoY_nspec (# of species)
! arraydimb = arraydima + 1 (for TVD/HLLC)
      arraydima = 6 + (nmat) + nmat+nspec
      arraydimb = 6 + (nmat) + nmat + nspec +1


! Allocate arrays for primitive variables from Globvar Module
      allocate( rho(-NG+1:NI+NG , -NG+1:NJ+NG,-NG+1:NK+NG) )
      allocate( u(-NG+1:NI+NG , -NG+1:NJ+NG,-NG+1:NK+NG) )
      allocate( v(-NG+1:NI+NG , -NG+1:NJ+NG,-NG+1:NK+NG) )
      allocate( w(-NG+1:NI+NG , -NG+1:NJ+NG,-NG+1:NK+NG) )
      allocate( p(-NG+1:NI+NG , -NG+1:NJ+NG,-NG+1:NK+NG) )
      allocate( T(-NG+1:NI+NG , -NG+1:NJ+NG,-NG+1:NK+NG,0:nmat) )
      allocate( phi(-NG+1:NI+NG , -NG+1:NJ+NG ,-NG+1:NK+NG, nmat) )
      allocate( psi(-NG+1:NI+NG , -NG+1:NJ+NG ,-NG+1:NK+NG, nmat) )
      allocate( rhomat(-NG+1:NI+NG , -NG+1:NJ+NG ,-NG+1:NK+NG, nmat) )
      allocate( soundspeed(-NG+1:NI+NG , -NG+1:NJ+NG,-NG+1:NK+NG) )

! Allocate array related shape correction/particles in Globvar Module
      allocate( phimoved(-NG+1:NI+NG , -NG+1:NJ+NG ,&
                        &-NG+1:NK+NG, num_particles) )
      allocate( phistart(-NG+1:NI+NG , -NG+1:NJ+NG ,&
                        &-NG+1:NK+NG, num_particles) )

      print*, '   Finished allocation for Globvar Module '

!  Allocate the arrays in Gridvariables module
      allocate(xc(NI,NJ,NK))
      allocate(yc(NI,NJ,NK))
      allocate(zc(NI,NJ,NK))
      allocate(vol(NI,NJ,NK))
      allocate(x(NI+1,NJ+1,NK+1))
      allocate(y(NI+1,NJ+1,NK+1))
      allocate(z(NI+1,NJ+1,NK+1))
      print*, '   Finished allocation for Gridvariables Module'

! Allocate arrays in hllc module
      allocate( ConsvarL(arraydimb) )
      allocate( ConsvarR(arraydimb) )
      allocate( URstar(arraydimb) )
      allocate( ULstar(arraydimb) )
      allocate( FRstar(arraydimb) )
      allocate( FLstar(arraydimb) )
      allocate( FR(arraydimb) )
      allocate( FL(arraydimb) )
      allocate( D(arraydimb) )
      print*, '   Finished allocation for HLLC Module'

! Allocate arrays in TVDFunc module
      allocate( netFlux(-NG+1:NI+NG , -NG+1:NJ+NG,&
                       &-NG+1:NK+NG, arraydima) )
      allocate( leftstateprim(arraydima) )
      allocate( rightstateprim(arraydima) )
      allocate( flx(arraydimb) )
      print*, '   Finished allocation for TVDFunc Module'

! Allocate arrays from RungKutt3 module
      allocate( temp(-NG+1:NI+NG , -NG+1:NJ+NG, &
                    &-NG+1:NK+NG, arraydima) )
      allocate( rhou(-NG+1:NI+NG , -NG+1:NJ+NG, -NG+1:NK+NG) )
      allocate( rhov(-NG+1:NI+NG , -NG+1:NJ+NG, -NG+1:NK+NG) )
      allocate( rhow(-NG+1:NI+NG , -NG+1:NJ+NG, -NG+1:NK+NG) )
      allocate( rhoE(-NG+1:NI+NG , -NG+1:NJ+NG, -NG+1:NK+NG) )
      allocate( rhoY(-NG+1:NI+NG , -NG+1:NJ+NG, -NG+1:NK+NG, 1:nspec) )
      allocate( state(arraydimb) )
      print*, '   Finished allocation for RungKutt3 Module'

! Allocate arrays from Shukla2010 module
      allocate( phinxcent(NI , NJ,NK,nmat) )
      allocate( phinycent(NI , NJ,NK,nmat) )
      allocate( phinzcent(NI , NJ,NK,nmat) )

      allocate( rhonxcent(NI , NJ,NK,nmat) )
      allocate( rhonycent(NI , NJ,NK,nmat) )
      allocate( rhonzcent(NI , NJ,NK,nmat) )

      allocate( netphi(NI , NJ,NK,nmat) )
      allocate( netrho(NI , NJ,NK,nmat) )
      allocate( tmprho(NI , NJ,NK,nmat) )

      allocate( temprho(NI , NJ,NK,2*nmat) )
      allocate( temprho3(-NG+1:NI+NG , -NG+1:NJ+NG, -NG+1:NK+NG) )

      allocate( temprhomat(-NG+1:NI+NG , -NG+1:NJ+NG,&
                         & -NG+1:NK+NG ,nmat) )
      allocate( temprhomat3(-NG+1:NI+NG , -NG+1:NJ+NG,&
                          & -NG+1:NK+NG , nmat) )

      allocate( tempphi(-NG+1:NI+NG , -NG+1:NJ+NG, -NG+1:NK+NG,nmat) )
      allocate( tempphi2(-NG+1:NI+NG , -NG+1:NJ+NG, -NG+1:NK+NG,nmat) )
      allocate( tempphi3(-NG+1:NI+NG, -NG+1:NJ+NG, -NG+1:NK+NG, nmat) )

      print*, '   Finished allocation for Shukla2010 Module'
      print*, 'FINISHED ALLOCATION OF ARRAYS '
      print*, ' '
      print*, ' '

      end subroutine alloc
!***********************************************************************
