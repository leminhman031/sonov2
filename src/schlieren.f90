!***********************************************************************
      subroutine makesch()
!***********************************************************************
! this subroutine makes numerical schlieren  using eqn (36) from
! Shukla et. al.; Journal of Computational Physics 
!     229 (2010) pp.7411-7439
!

      use globalvar
      use gridvariables
      use Shukla2010, only: rho_x,rho_y,rho_z
      use RungKutt3, only: temp

      real :: fluidgam, fluidpinf,fluidc,drhomax

      temp = 0.00
      drhomax = 0.00
      do k=1,NK
      do j=1,NJ
      do i=1,NI
         rho_x = 0.5*(rho(i+1,j,k) - rho(i-1,j,k))/delx
         rho_y = 0.5*(rho(i,j+1,k) - rho(i,j-1,k))/dely
         rho_z = 0.5*(rho(i,j,k+1) - rho(i,j,k-1))/delz
         temp(i,j,k,1) = sqrt(rho_x**2 + rho_y**2 + rho_z**2)
         if(drhomax<temp(i,j,k,1)) then 
             drhomax=temp(i,j,k,1) 
         endif
      enddo 
      enddo 
      enddo 


      do k=1,NK
      do j=1,NJ
      do i=1,NI

         dumgam = 0.00
         dumpinf = 0.00
         fluidc = 0.00
         do m=1, (nmat)
            dumgam = dumgam + phi(i,j,k,m)/( matprop(2,m) - 1)
            dumpinf = dumpinf + &
           & phi(i,j,k,m)*matprop(2,m)*matprop(3,m)/(matprop(2,m) - 1)
            fluidc = fluidc + phi(i,j,k,m)*matprop(1,m)
         enddo


         fluidc = fluidc + lastphi*matprop(1,nmat) ! fluid specific heat
         fluidgam = (1+dumgam)/dumgam     ! fluid gamma
         fluidpinf = dumpinf/(1+dumgam)  ! fluid pinf

         if(fluidgam>0.5*(matprop(2,1)+matprop(2,2)) ) then
             temp(i,j,k,2) = exp(-20.0*temp(i,j,k,1)/drhomax)
         else 
             temp(i,j,k,2) = exp(-100.0*temp(i,j,k,1)/drhomax)
         endif 

         if(drhomax == 0.00) then 
             temp(i,j,k,2) = 0.0 
         endif 

      enddo 
      enddo 
      enddo 



      end subroutine makesch
!***********************************************************************
